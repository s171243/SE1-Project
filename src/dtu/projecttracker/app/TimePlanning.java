package dtu.projecttracker.app;

import java.util.Date;
//@David

public interface TimePlanning {
	/*
	 * Simple getters
	 */
	public Date getStartDate();

	//public Date getEndDate();
	
	public int getSpentTime();
	
	public int getBudgetedTime();

	/*
	 * Simple setters
	 */
	public void setStartDate(String startTime) throws OperationNotAllowedException;;

	public void setEndDate(String endTime) throws OperationNotAllowedException;

	public void setBudgetedTime(String budgetedTime) throws OperationNotAllowedException;
} // class