package dtu.projecttracker.app;

import java.util.ArrayList;
import java.util.Date;

// @author Deina
public class PersonalActivity extends Activity {
	// inherits all methods and constructor from activity
	public PersonalActivity(App app, String employeeID) throws Exception {
		super(app);
		
		// gets the creator of the personal activity
		Employee creator = model.getEmployee(employeeID);
		this.assignEmployee(creator);
	}
}
