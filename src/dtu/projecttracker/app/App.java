package dtu.projecttracker.app;

import java.lang.Thread;

/**
 * This class is the root container for Employee and Project objects.
 * 
 * @author Asbjørn Olling
 *
 */

public class App {
	// MVC objects
	private Model model;
	private ConsoleView viewer;
	private ConsoleController controller;
	private Thread controllerThread;
	private String projectID;
	private String employeeID;
	private String activityID;

	// main method
	// just create an instance of the root object
	public static void main(String[] args) throws Exception {
		App app = new App();
		Model model = app.getModel();
		
		// hardcoded employee
		String empID = "AAAA";
		Employee empl = model.createEmployee(empID);
		Employee empl1 = model.createEmployee("ABCD");
		Employee empl2 = model.createEmployee("AAAB");
		Employee empl3 = model.createEmployee("TEST");
		Employee empl4 = model.createEmployee("PERS");
		Employee empl5 = model.createEmployee("DEIN");
		Employee empl6 = model.createEmployee("CHRI");
		model.logIn(empID);
		
		// project
		Project proj = model.createProject();
		Project proj2 = model.createProject();
		
		// set projectLeader
		proj.assignProjectLeader(empID);
		proj.setName("Test");
		
		// Create activities
		Activity act1 = proj.createProjectActivity();
		Activity act2 = proj.createProjectActivity();
		Activity act3 = proj.createProjectActivity();
		Activity act4 = proj.createProjectActivity();
		Activity act5 = proj.createProjectActivity();
		Activity act6 = proj.createProjectActivity();
		
		// assign employees to project
		
		act1.assignEmployee(empl1);
		act1.assignEmployee(empl2);
		act1.assignEmployee(empl3);
		act1.assignEmployee(empl4);
		act1.assignEmployee(empl5);
		
		act2.assignEmployee(empl2);
		act2.assignEmployee(empl3);
		act2.assignEmployee(empl4);
		act2.assignEmployee(empl5);
		act2.assignEmployee(empl6);
		
		act3.assignEmployee(empl1);
		act3.assignEmployee(empl2);
		act3.assignEmployee(empl3);
		act3.assignEmployee(empl4);
		act3.assignEmployee(empl5);
		
		// setName
		act1.setName("Kravsspecifikation");
		act2.setName("Projektledelse");
		act3.setName("Analyse");
		act4.setName("Design");
		act5.setName("Programmering");
		act6.setName("Testing");
		
		// setStartDate
		act1.setStartDate("2018-04-13");
		act2.setStartDate("2018-02-15");
		act3.setStartDate("2018-01-12");
		act4.setStartDate("2018-03-12");
		act5.setStartDate("2018-02-12");
		act6.setStartDate("2018-01-12");
		
		// setEndDate
		act1.setEndDate("2018-05-14");
		act2.setEndDate("2018-06-17");
		act3.setEndDate("2018-07-19");
		act4.setEndDate("2018-08-24");
		act5.setEndDate("2018-09-24");
		act6.setEndDate("2018-10-24");
		
		// sets system back to initial situation
		model.setLoggedInUser(null);
		model.setLogInStatus(false);
		
		// peronal activity
		model.createPersonalActivity(empID);
	}

	// constructor
	public App() {
		this.model = new Model(this);
		this.viewer = new ConsoleView(this);

		// start controller thread
		this.controller = new ConsoleController(this);
		this.controllerThread = new Thread(this.controller);
		this.controllerThread.start();
	} // constr

	/*
	 * Simple getters
	 */
	public Model getModel() {
		return this.model;
	}

	public ConsoleView getViewer() {
		return this.viewer;
	}

	public ConsoleController getController() {
		return this.controller;
	}

	/*
	 * Simple setters
	 */

	/*
	 * Methods for tests
	 */

	public String getProjectID() {
		return this.projectID;
	}

	public String getEmployeeID() {
		return this.employeeID;
	}

	public String getActivityID() {
		return this.activityID;
	}

	

	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public void setActivityID(String activityID) {
		this.activityID = activityID;
	}

	/*
	 * End test methods
	 */

} // App class end