package dtu.projecttracker.app;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

// @author 
public class Project implements TimePlanning {
	// other application objects
	private final App app;
	private Model model;

	// project attributes
	private String projectID; // projectID = year + zero-padded serial
	private Employee projectLeader;
	private String projectName;

	// employees and activities
	private ArrayList<Employee> employees;
	private ArrayList<Activity> activities;

	// time attributes
	private Date startDate;
	private Date endDate;
	private int budgetedTime;

   //@David and Deina
	public Project(App app) {
		this.app = app;
		this.model = app.getModel();

		// init arraylists
		this.employees = new ArrayList<Employee>();
		this.activities = new ArrayList<Activity>();

		// add to list of all projects
		model.addProject(this);

		// create project ID
		// YYYY-<6 DIGIT SERIAL>
		String year = "" + model.now.get(Calendar.YEAR);
		this.projectID = year + "-" + app.getModel().getNextProjectSerial();

		// default budgetedTime to 0 hours
		this.budgetedTime = 0;

		// default start/end dates
		this.startDate = model.getToday();
		this.endDate = model.getTomorrow();

		// default projectName: empty string
		this.projectName = new String();
	} // end constructor

	/*
	 * Create methods
	 */

	// If user is the projectLeader, add an activity on this project
	// @author Deina
	public ProjectActivity createProjectActivity() throws Exception {
		if (model.getLoggedInUser() == this.projectLeader) {
			// creates project activity
			ProjectActivity newActivity = new ProjectActivity(this.app);
			
			// add prjocect to activity
			newActivity.setProject(this);

			// adds project activity to list of all activities
			this.addProjectActivity(newActivity);
			return newActivity;
		}
		// else throw exception
		else {
			String errorMessage = "You’re not allowed to create an activity on this project";
			// model.setError(errorMessage);
			throw new OperationNotAllowedException(model, errorMessage);
		}
	}

	/*
	 * Add methods
	 */

	// adds project activity to list of activities
	// @author David
	public void addProjectActivity(ProjectActivity activity) {
		this.activities.add(activity);
	}

	/*
	 * Complex getters
	 */

	// gets total spent time by accumulating all time on related activities
	// @author Deina
	public int getSpentTime() {
		int totalSpentTime = 0;
		for (Activity act : activities) {
			totalSpentTime += act.getSpentTime();
		}
		return totalSpentTime;
	}

	/*
	 * Simple getter methods.
	 */
	public int getBudgetedTime() {
		return this.budgetedTime;
	}

	public String getID() {
		return projectID;
	}

	public ArrayList<Employee> getEmployees() {
		return employees;
	}

	public Employee getProjectLeader() {
		return projectLeader;
	}

	public String getName() {
		return projectName;
	}

	public ArrayList<Activity> getActivities() {
		return this.activities;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	/*
	 * HAS-METHOD
	 */

	// returns whether or not an employee with ID is assigned to a project
	// @author Deina
	public boolean hasEmployee(String employeeID) {
		// if there are no employees, the project doesn't have any
		for (Employee e : employees) {
			if (e.getID().equals(employeeID)) {
				return true;
			}
		}

		// if it reaches this point, it does not exist
		return false;
	}

	/*
	 * Simple setter methods.
	 */
	// @author Deina
	public void setBudgetedTime(String budgetedTimeString) throws OperationNotAllowedException {
		// sets string to budgeted time
		this.budgetedTime = model.parseNum(budgetedTimeString);
	}

	// assign an employee to the project
	// @author David
	public void assignEmployee(String newEmployeeID) throws OperationNotAllowedException {
		// DbC
		assert newEmployeeID != null && newEmployeeID.length() == 4 : "precondition violated";
		
		// check existence, assigment status and permission levels
		if (model.hasEmployee(newEmployeeID)) {
			Employee newEmployee = model.getEmployee(newEmployeeID);
			if (!hasEmployee(newEmployeeID)) {
				if (model.getLoggedInUser() == projectLeader) {
					newEmployee.addProject(this);
					employees.add(newEmployee);
					// DbC
					assert hasEmployee(newEmployeeID) : "postconditions violated";
					
					
				// handle various errors
				} else {
					throw new OperationNotAllowedException(model, "You’re not a project leader and can’t add users");
				}
			}
			else {
				throw new OperationNotAllowedException(model, "The employee is already added to the project");
			}
		}
		else {
			throw new OperationNotAllowedException(model, "The employee doesn’t exist");
		}
	} // end method

	//@author David
	public void setID(String newID) {
		this.projectID = newID;
	}

	// sets the name of the project
	// @author David
	public void setName(String newName) throws OperationNotAllowedException {
		// if someone is logged in, continue
		if (model.getLoginStatus()) {

			// given the user is projectLeader of the project
			if (model.getLoggedInUser() == getProjectLeader()) {

				// DbC
				assert newName != null && model.getLoginStatus()
					&& model.getLoggedInUser() == getProjectLeader() : "Preconditions violated";

				// if the user is logged in, set the name
				this.projectName = newName;

				// DbC
				assert this.projectName == newName : "Postconditions violated";
			}

			// else, throw exception
			else {
				throw new OperationNotAllowedException(model, "You’re not allowed to change the name of this project");
			}

		}
		// else, throw exception
		else {
			throw new OperationNotAllowedException(model, "You’re not logged in");
		}
	}

	// assign projectLeader
	// @author David
	public void assignProjectLeader(String employeeID) throws OperationNotAllowedException {
		// get employee and set as leader
		Employee employee = model.getEmployee(employeeID);
		this.setProjectLeader(employee);

		// if the employee isn't already added to the project, add them.
		// doesn't use assignEmployee - b/c it requires login status and PL status
		if (!hasEmployee(employee.getID())) {
			employees.add(employee);
			employee.addProject(this);
		}
	}

	// set the projectLeader
	// @author David
	public void setProjectLeader(Employee employee) {
		this.projectLeader = employee;
	}

	// sets the start date
	// @author Deina
	public void setStartDate(String startDate) throws OperationNotAllowedException {
		Date date = model.parseString(startDate);
		// ensure that start date is before the end date
		if (date.before(endDate)) {
			this.startDate = date;
		} 
		else {
			throw new OperationNotAllowedException(model, "The start date is after the end date");
		}
	}

	// sets the end date
	// @author Deina
	public void setEndDate(String endTime) throws OperationNotAllowedException {
		Date endDate = model.parseString(endTime);
		// ensure that end date preceeds start date
		if (endDate.after(startDate)) {
			this.endDate = endDate;
		} 
		else {
			throw new OperationNotAllowedException(model, "The end date is before the start date");
		}
	}

} // end Project class
