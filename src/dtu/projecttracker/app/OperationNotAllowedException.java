package dtu.projecttracker.app;

// @author David
public class OperationNotAllowedException extends Exception {

	private String string;
	private Model model;
	
	// this Exception mostly exists to set the model's error/message
	public OperationNotAllowedException(Model model, String string) {
		this.model = model;
		this.setMessage(string);
		model.setError(string);
	}

	public void setMessage(String string) {
		this.string = string;
	}

}
