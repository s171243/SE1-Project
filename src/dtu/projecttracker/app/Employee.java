package dtu.projecttracker.app;

import java.util.ArrayList;

public class Employee {
	// other app objects
	private App app;
	private Model model;
	
	// employee attributes
	private String employeeID;
	
	// encapsulated objects
	private ArrayList<Activity> activities;
	private ArrayList<Project> projects;
	
	// @author Deina
	public Employee(App app, String employeeID) throws Exception {
		this.app = app;
		this.model = app.getModel();
		
		// add this object to list of all employees
		this.model.addEmployee(this);
		
		// init lists
		this.activities = new ArrayList<Activity>();
		this.projects = new ArrayList<Project>();
		
		setID(employeeID);
	}
	
	/*
	 * Add & assign methods
	 */
	

	// @author David
	public PersonalActivity createActivity() throws Exception {
		PersonalActivity newAct = new PersonalActivity(this.app, this.employeeID);
		return newAct;
	}
	// @author David
	// add an activity to list of employees activities
	public void addActivity(Activity activity) {
		this.activities.add(activity);
	}
	// @author David
	// add a project to list of employees projects
	public void addProject(Project project) {
		this.projects.add(project);
	}

	/*
	 * Simple getters
	 */
	public String getID() {
		return employeeID;
	}

	public ArrayList<Activity> getActivities() {
		return this.activities;
	}
	
	public ArrayList<Project> getProjects() {
		return this.projects;
	}
		
	/*
	 * Complex setters
	 */
	
	//@author Deina
	public void setID(String employeeID) throws Exception {
		// if the employee length is 4 characters, continue
		this.employeeID = employeeID;
	}
	
} // end class