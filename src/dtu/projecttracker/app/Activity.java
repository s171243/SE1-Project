package dtu.projecttracker.app;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
// java imports
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

// @author 
public abstract class Activity implements TimePlanning {
	// other app objects
	protected Model model;
	DateFormat format = new SimpleDateFormat("y-MM-dd", Locale.ENGLISH);

	// activity attributes
	protected String activityName = ""; // human-readable name
	protected String activityID; // unique ID
	private ArrayList<Employee> employees; // employees on this activity
	private Map<String, Integer> timeTable; // timetable of registered time

	// time objects
	private Date startDate;
	private Date endDate;
	private int budgetedTime; // in hours

	// constructor is called from inherited objects
	//@author Christian
	public Activity(App app) {
		this.model = app.getModel();

		// init lists
		employees = new ArrayList<Employee>();
		timeTable = new HashMap<String, Integer>();

		// time defaults
		this.startDate = this.model.getToday();
		this.endDate = this.model.getTomorrow();

		// sets the activity serial
		this.activityID = app.getModel().getNextActivitySerial();

		// adds the activity to list of activities
		model.addActivity(this);
	}

	/*
	 * ADD methods
	 */
	// register time spent as a specific user
	// handle strings with mixed number / letter input
	//@author Christian
	public void registerTimeSpent(String timeSpentString, String employeeID) throws OperationNotAllowedException {
		// checks if the employee is added to the activity
		if (hasEmployee(employeeID)) {
			int timeSpent = model.parseNum(timeSpentString);

			// if the employee is present in the time-table, add time to the employees total
			if (timeTable.containsKey(employeeID)) {
				timeTable.put(employeeID, timeTable.get(employeeID) + timeSpent);
			}
			// else, create the employee in the time-table
			else {
				timeTable.put(employeeID, timeSpent);
			}
		}

		// handles error
		else {
			throw new OperationNotAllowedException(model, "You are not assigned to this activity");
		}
	}

	/*
	 * HAS methods
	 */
	// checks if the employee is assigned to the project
	// primarily used for testing
	//@author Christian
	public boolean hasEmployee(String employeeID) {
		for (Employee emp : employees) {
			// Checks if the employee ID's match
			if (emp.getID().equals(employeeID)) {
				return true;
			}
		}
		// code only reaches this point if employee not found
		return false;
	}

	/*
	 * ASSIGNs
	 */

	// adds employee to list of employees
	// @author Christian
	public void assignEmployee(Employee employee) throws OperationNotAllowedException {
		if (!this.hasEmployee(employee.getID())) {
			this.employees.add(employee);
			employee.addActivity(this);
			if (!this.timeTable.containsKey(employee.getID())) {
				this.registerTimeSpent("0", employee.getID());
			}
		} else {
			throw new OperationNotAllowedException(model, "Employee already assigned to this activity");
		}
	}

	/*
	 * Complex getters
	 */

	// Gets total amount of time spent on activity
	// @author Christian
	public int getSpentTime() {
		int totalSpentTime = 0;
		for (int time : timeTable.values()) {
			totalSpentTime += time;
		}
		return totalSpentTime;
	}

	// gets time spent from a singular employee
	// @author Christian
	public int getSpentTime(String employeeID) {
		return timeTable.get(employeeID);
	}

	/*
	 * Simple Getters
	 */
	public String getName() {
		return activityName;
	}

	public String getID() {
		return activityID;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	// re-formats the date to a string and returns it
	// @author Christian
	public String getStartDateAsString() {
		return format.format(startDate);
	}

	// re-formats the date to a string and returns it
	// @author Christian
	public String getEndDateAsString() {
		return format.format(endDate);
	}

	public int getBudgetedTime() {
		return budgetedTime;
	}

	public ArrayList<Employee> getEmployees() {
		return this.employees;
	}

	/*
	 * Simple Setters
	 */

	// @author Christian
	public void setName(String activityName) throws OperationNotAllowedException {
		// if the logged in user is assigned to this activity
		if (hasEmployee(model.getLoggedInUser().getID())) {
			this.activityName = activityName;
		}

		// else, throw exception
		else {
			throw new OperationNotAllowedException(model, "You’re not allowed to change the name of this activity");
		}
	}

	// @author Christian
	public void setID(String activityID) {
		this.activityID = activityID;
	}

	// sets the start date based on a string
	// @author Christian
	public void setStartDate(String startDate) throws OperationNotAllowedException {
		Date date = model.parseString(startDate);
		
		if (date.before(endDate)) {
			this.startDate = date;
		}

		// else, throw exception
		else {
			throw new OperationNotAllowedException(model, "The start date is after the end date");
		}
	}

	// sets the end date based on a string
	// @author Christian
	public void setEndDate(String endDate) throws OperationNotAllowedException {
		Date date = model.parseString(endDate);

		if (date.after(startDate)) {
			this.endDate = date;
		}

		// else, throw exception
		else {
			throw new OperationNotAllowedException(model, "The end date is before the start date");
		}

	}

	// sets the budgeted time of activity
	// @author Christian
	public void setBudgetedTime(String budgetedTimeString) throws OperationNotAllowedException {
		this.budgetedTime = model.parseNum(budgetedTimeString);
	}
} // class
