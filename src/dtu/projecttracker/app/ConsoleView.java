package dtu.projecttracker.app;

// java imports
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/*
 * This class is responsible for displaying information to the user.
 * @author Asbjørn
 */
public class ConsoleView {
	private App app;
	private Model model;
	private DateFormat dateFormat;
	
	private final String[] banner = {
		" _____           _         _   _____             _           ",
		"|  _  |___ ___  |_|___ ___| |_|_   _|___ ___ ___| |_ ___ ___ ",
		"|   __|  _| . | | | -_|  _|  _| | | |  _| .'|  _| '_| -_|  _|",
		"|__|  |_| |___|_| |___|___|_|   |_| |_| |__,|___|_,_|___|_|  ",
		"              |___|                                          "
	};
	
	public ConsoleView(App app) {
		this.app = app;
		this.model = this.app.getModel();
		this.dateFormat = model.format;
		
		// print banner
		this.printArray(banner);
	}
	
	/*
	 * Prompts
	 */
	// text before the login field
	// @author Asbjørn
	public void loginPrompt() {
		System.out.print("Login: ");
	}
	
	// text before the command input space
	public void commandPrompt() {
		System.out.print("Enter command: ");
	}
	
	/*
	 * Simple user feedback
	 */
	// user feedback for valid login
	// @author Asbjørn
	public void loginSuccess() {
		System.out.println("Sucessfully logged in!\n");
	}

	// let the user now that the action succeded
	// and print the resulting activity ID
	// @author Asbjørn
	public void createdActivity(String activityID) {
		System.out.println("Successfully created activity with ID: " + activityID);
	}
	
	// let the user know that they successfully made a project
	// and print the resulting ID
	// @author Asbjørn
	public void createdProject(String projectID) {
		System.out.println("Successfully created project with ID: " + projectID);
	}
	
	// let the user know that we're done
	// @author Asbjørn
	public void terminate() {
		System.out.println("Terminating program... Goodbye!");
	}
	
	/*
	 * ERRORS
	 */
	// generic error
	// @author Asbjørn
	public void genericError(String message) {
		System.out.println("[ERROR] " + message);
	}

	// feedback syntax errors
	// @author Asbjørn
	public void badSyntax() {
		System.out.println("[ERROR] Bad syntax. Maybe take a look at the help page?");
	}

	// user feedback for invalid login
	// @author Asbjørn
	public void loginFailure() {
		System.out.println("[ERROR] Could not find user, try again.\n");
	}
	
	// if it's an invalid command
	// @author Asbjørn
	public void commandNotFound() {
		System.out.println("[ERROR] Command not found. Try again, or type \"help\".");
	}
	
	// print every line from a string array
	// @author Asbjørn
	private void printArray(String[] array) {
		for (String line : array) {
			System.out.println(line);
		}
	}
	
	/* 
	 * Print very pretty table of strings.
	 * The headers array can be of different lengths.
	 * The data array contains string arrays, where each 
	 * individual array only contains strings of the same length.
	 * @author Asbjørn
	 */
	private void printTable(String[] headers, String[][] data) {
		// get char widths for each by looking at the data 
		int[] fieldWidths = new int[data.length];
		for (int i = 0; i < fieldWidths.length; i++) {
			fieldWidths[i] = data[i][0].length();
		}

		// make column headers
		String headersLine = new String();
		for (int i = 0; i < headers.length; i++) {
			// get header and width
			String header = headers[i];
			int desiredWidth = fieldWidths[i];
			// space-pad header if too short
			while (header.length() < desiredWidth) {
				header += " ";
			}
			// add table bars and add to line
			headersLine += "| " + header + " ";
		}
		headersLine += "|";

		// make vertical header separator
		String separatorLine = new String();
		for (char c : headersLine.toCharArray()) {
			if (c == '|') {
				separatorLine += "+";
			} else {
				separatorLine += "-";
			}
		}

		// print header w/ frame
		System.out.println(separatorLine);
		System.out.println(headersLine);
		System.out.println(separatorLine);

		// print actual data
		int lines = data[0].length;
		int fields = headers.length;
		for (int i = 0; i < lines; i++) {
			String dataLine = new String();
			for (int j = 0; j < fields; j++) {
				String dataString = data[j][i];
				// space-pad datastring if shorter than header
				while (dataString.length() < headers[j].length()) {
					dataString = " " + dataString;
				}
				dataLine += "| " + dataString + " ";
			}
			dataLine += "|";
			System.out.println(dataLine);
		}

		// print the very last line
		System.out.println(separatorLine);
	} // printTable

	/*
	 * Prints table of all current projects, with detailed information
	 * about dates, budgeted time, employees assigned, etc.
	 * Optionally supply an employeeID, to filter results by.
	 * If employeeID is left empty, all projects are shown.
	 * @author David
	 */
	public void viewAllProjects(String employeeID) throws Exception {
		ArrayList<Project> allProjects;
		String header = new String("\n");
		if (employeeID != "") {
			allProjects = model.getEmployee(employeeID).getProjects();
			header += "All projects for employee " + employeeID;
		} else {
			allProjects = model.getProjects();
			header += "All current projects:";
		}
		int numberOfProjects = allProjects.size();
		
		// arrays containing info to fill the table
		String[] projectIDs = new String[numberOfProjects];
		String[] projectNames = new String[numberOfProjects];
		String[] startDates = new String[numberOfProjects];
		String[] endDates = new String[numberOfProjects];
		String[] numbersOfEmployees = new String[numberOfProjects];
		String[] budgetedTime = new String[numberOfProjects];
		String[] spentTime = new String[numberOfProjects];

		// gather data from all projects
		int counter = 0;
		for (Project project : allProjects) {
			// project ID: YYYY-XXXXXX
			// always 11 chars
			projectIDs[counter] = project.getID();
			
			// project name is arbitrary string
			String projectName = project.getName();
			// cut to 16 chars if longer
			if (projectName.length() > 16) {
				projectName = projectName.substring(0, 16);
			}
			// add spaces if shorter
			while (projectName.length() < 16) {
				projectName += " ";
			}
			projectNames[counter] = projectName;
			
			// start date: YYYY-MM-DD
			// always 10 chars
			startDates[counter] = dateFormat.format(project.getStartDate());

			// end date: YYYY-MM-DD
			// always 10 chars
			endDates[counter] = dateFormat.format(project.getEndDate());
			
			// number of employees: string zero-padded to three digits + " Employees"
			// always 14 chars
			numbersOfEmployees[counter] = String.format("%03d", project.getEmployees().size()) + " Employees";

			// budgeted time: padded to four digits + " Hours"
			// always ten chars
			budgetedTime[counter] = String.format("%03d", project.getBudgetedTime()) + " Hours";
			
			// spent time: string zero-padded to three digits + " Hours"
			// always ten chars
			spentTime[counter] = String.format("%03d", project.getSpentTime()) + " Hours";
			
			// increment counter, to get next position in arrays
			counter++;
		}

		// assemble data array
		String[][] data = {
				projectIDs,
				projectNames,
				startDates,
				endDates,
				numbersOfEmployees,
				budgetedTime,
				spentTime
		};
		String[] headers = {
				"Project ID",
				"Project Name",
				"Start Date",
				"End Date",
				"Number Of Employees",
				"Budgeted Time",
				"Spent Time"
		};

		// print the table
		System.out.println(header);
		this.printTable(headers, data);
	} // project overview

	/*
	 * Prints table of all current employees, with information about
	 * their ID, their number of current activities and number of current projects.
	 * @author Deina
	 */
	public void viewAllEmployees() {
		int numberOfEmployees = model.getEmployees().size();
		
		String[] employeeIDs = new String[numberOfEmployees];
		String[] numberOfActivities = new String[numberOfEmployees];
		String[] numberOfProjects = new String[numberOfEmployees];
		
		// assemble data
		int counter = 0;
		for (Employee emp : model.getEmployees()) {
			// always four chars
			employeeIDs[counter] = emp.getID();
			
			// zero-padded to two chars + " Activities"
			// always 13 chars
			numberOfActivities[counter] = String.format("%02d", emp.getActivities().size()) + " Activities";
			
			// zero-padded to two chars + " Projects"
			// always 11 chars
			numberOfProjects[counter] = String.format("%02d", emp.getProjects().size()) + " Projects";
			
			counter++;
		}
		
		// package data
		String[][] data = {
				employeeIDs,
				numberOfActivities,
				numberOfProjects
		};
		String[] headers = {
				"Employee ID",
				"Number of Activities",
				"Number of Projects"
		};
		
		// print the table
		System.out.println("Overview of all current employees:");
		this.printTable(headers, data);
	} // employees overview
	
	/*
	 * Prints table of a current activities, with information about
	 * the fields of the object.
	 * Optionally specify a projectID or employeeID to limit activites.
	 * If empty string is supplied, just print all
	 * @author Asbjørn
	 */
	public void viewAllActivities(String id) throws Exception {
		ArrayList<Activity> activities;
		String header = new String("\n");
		// if project ID supplied
		if (id.length() == 11) {
			activities = model.getProject(id).getActivities();
			header += "All activities for project " + id;
			
		// if employee id supplied
		} else if (id.length() == 4) {
			activities = model.getEmployee(id).getActivities();
			header += "All activities for employee " + id;
		}
		
		// if no id supplied
		else {
			activities = model.getActivities();
			header += "All current activities:";
		}
		
		// init lists
		int numberOfActivities = activities.size();
		if (numberOfActivities > 0) {
			String[] activityIDs = new String[numberOfActivities];
			String[] activityNames = new String[numberOfActivities];
			String[] startDates = new String[numberOfActivities];
			String[] endDates = new String[numberOfActivities];
			String[] employees = new String[numberOfActivities];
			String[] budgetedTime = new String[numberOfActivities];
			String[] spentTime = new String[numberOfActivities];
			
			// gather data
			int counter = 0;
			for (Activity act : activities) {
				// always six chars, zero padded
				activityIDs[counter] = act.getID();

				// activity name is an arbitrary length string
				String name = act.getName();
				// cut to 16 chars if longer
				if (name.length() > 16) {
					name = name.substring(0, 16);
				}
				while (name.length() < 16) {
					name += " ";
				}
				activityNames[counter] = name;
				
				// start date: YYYY-MM-DD
				// always 10 chars
				startDates[counter] = act.getStartDateAsString();
				
				// end date: YYYY-MM-DD
				// always 10 chars
				endDates[counter] = act.getEndDateAsString();
				
				// number of employees: padded to two digits + " Employees"
				employees[counter] = String.format("%02d", act.getEmployees().size());
				
				// budgeted time: padded to four digits + " Hours"
				// always ten chars
				budgetedTime[counter] = String.format("%03d", act.getBudgetedTime()) + " Hours";
				
				// spent time: string zero-padded to three digits + " Hours"
				// always ten chars
				spentTime[counter] = String.format("%03d", act.getSpentTime()) + " Hours";
				
				counter++;
			}
			
			// package data
			String[][] data = {
				activityIDs,
				activityNames,
				startDates,
				endDates,
				employees,
				budgetedTime,
				spentTime
			};
			String[] headers = {
				"Activity IDs",
				"Name",
				"Start Dates",
				"End Dates",
				"Number of employees",
				"Budgeted Time",
				"Spent Time"
			};
			
			// print data
			System.out.println(header);
			printTable(headers, data);
		}
	} // view-all activities
	
	/*
	 * Lists all employees, along with the number of activities they have planned 
	 * in a specific week.
	 * @author Asbjørn
	 */
	public void viewWeek(String date) {
		// parse input
		String[] inputArray = date.split("-");
		int year = Integer.parseInt(inputArray[0]);
		int month = Integer.parseInt(inputArray[1]);
		int day = Integer.parseInt(inputArray[2]);
		
		// generate date
		Date givenDate = new Date(year - 1900, month - 1, day);
		long givenDateUnix = givenDate.getTime(); // get date in unix time (milis)
		
		// find delimiting days of week
		int daysFromMon; 
		if (givenDate.getDay() != 0) {
			daysFromMon = givenDate.getDay() - 1;
		} else {
			// handle sundays being first day of week 
			// according to stupid java
			daysFromMon = 7;
		}
		
		// get unix time for the delimiting days of that week
		long monday = givenDateUnix - (daysFromMon * 24 * 60 * 60 * 1000);
		long sunday = monday + (7 * 24 * 60 * 60 * 1000);
		
		// init data arrays
		int noOfEmployees = model.getEmployees().size();
		String[] employeeIDs = new String[noOfEmployees];
		String[] noOfProjects = new String[noOfEmployees];
		String[] noOfActivities = new String[noOfEmployees];
		
		// gather data
		int counter = 0;
		for (Employee emp : model.getEmployees()) {
			employeeIDs[counter] = emp.getID();
			
			// count active projects in the given week
			int projects = 0;
			for (Project proj : emp.getProjects()) {
				long startDate = proj.getStartDate().getTime(); // in unix milis
				long endDate = proj.getEndDate().getTime(); // in unix milis
				boolean projectInWeek = (startDate < monday && sunday < endDate) 
										|| (startDate < monday && monday < endDate) 
										|| (startDate < sunday && sunday < endDate);
				if (projectInWeek) {
					projects++;
				}
			}
			noOfProjects[counter] = String.format("%02d", projects) + " Projects";

			// count active activities in the given week
			int activities = 0;
			for (Activity act : emp.getActivities()) {
				long startDate = act.getStartDate().getTime();
				long endDate = act.getEndDate().getTime();
				boolean activityInWeek = (startDate < monday && sunday < endDate) 
										|| (startDate < monday && monday < endDate) 
										|| (startDate < sunday && sunday < endDate);
				if (activityInWeek) {
					activities++;
				}
			}
			noOfActivities[counter] = String.format("%02d", activities) + " Activites";
			
			// increment and loop
			counter++;
		}
		
		// package data
		String[][] data = {
			employeeIDs,
			noOfProjects,
			noOfActivities
		};
		String[] headers = {
				"Employee ID",
				"Number of projects",
				"Number of activities"
		};
		
		// print data
		System.out.println("Employee activity in the week containing date " + date);
		printTable(headers, data);
	} // view-week
	
	// view all fields in a project
	// @author Deina
	public void viewProject(String projectID) throws Exception {
		Project proj = model.getProject(projectID);
		
		// handle null project leader
		String projLeader = new String();
		if (proj.getProjectLeader() != null) {
			projLeader = proj.getProjectLeader().getID();
		}
		
		System.out.println("Project ID: \t" + proj.getID());
		System.out.println("Project Name: \t" + proj.getName());
		System.out.println("Leader: \t" + projLeader);
		System.out.println("Employees: \t" + proj.getEmployees().size());
		System.out.println("Activities: \t" + proj.getActivities().size());
		System.out.println("Start date: \t" + dateFormat.format(proj.getStartDate()));
		System.out.println("End date: \t" + dateFormat.format(proj.getEndDate()));
		System.out.println("Budgeted time: \t" + proj.getBudgetedTime() + " Hours");
		System.out.println("Spent time: \t" + proj.getSpentTime() + " Hours");
		
		// employees and time tables
		int noOfEmployees = proj.getEmployees().size();
		if (noOfEmployees > 0) {
			// init lists
			String[] employeeIDs = new String[noOfEmployees];
			String[] timeSpent = new String[noOfEmployees];
			
			// gather data
			int counter = 0;
			for (Employee emp : proj.getEmployees()) {
				String empID = emp.getID();
				
				// count total time spent
				// across all activities in project
				int time = 0;
				for (Activity act : proj.getActivities()) {
					if (act.hasEmployee(empID)) {
						time += act.getSpentTime(empID);
					}
				}
				
				// deposit data and loop
				employeeIDs[counter] = empID;
				timeSpent[counter] = String.format("%03d", time);
				counter++;
			}
			
			// package data
			String[][] data = {
					employeeIDs,
					timeSpent
			};
			String[] headers = {
					"Employee ID",
					"Time spent on project"
			};
			
			// print timetable
			System.out.println("\nTimetable for employees on project " + proj.getID() + ":");
			printTable(headers, data);
		}
		
		// print all activities
		this.viewAllActivities(proj.getID());
	} // project view
	
	// view all fields in an activity
	// @author Asbjørn
	public void viewActivity(String activityID) throws Exception {
		Activity act = model.getActivity(activityID);
		System.out.println("Activity ID: \t" + act.getID());
		System.out.println("Activity Name: \t" + act.getName());
		System.out.println("Employees: \t" + act.getEmployees().size());
		System.out.println("Start date: \t" + dateFormat.format(act.getStartDate()));
		System.out.println("End date: \t" + dateFormat.format(act.getEndDate()));
		System.out.println("Budgeted time: \t" + act.getBudgetedTime() + " Hours");
		System.out.println("Spent time: \t" + act.getSpentTime() + " Hours");
		
		// print timetable
		int noOfEmployees = act.getEmployees().size();
		if (noOfEmployees > 0) {

			// init arrays
			String[] employeeIDs = new String[noOfEmployees];
			String[] timeSpent = new String[noOfEmployees];
			
			// gather data
			int counter = 0;
			for (Employee emp : act.getEmployees()) {
				employeeIDs[counter] = emp.getID();
				timeSpent[counter] = "" + act.getSpentTime(emp.getID());
				counter++;
			}

			// package data
			String[][] data = {
					employeeIDs,
					timeSpent
			};
			String[] headers = {
					"Employee ID",
					"Time spent"
			};
			
			// print table
			System.out.println("Timetable for activity " + act.getID());
			printTable(headers, data);
		}
	} // view activity
	
	// view all fields on an employee
	// @author Deina
	public void viewEmployee(String employeeID) throws Exception {
		Employee emp = model.getEmployee(employeeID);
		System.out.println("Employee ID: \t" + emp.getID());
		int noOfProjects = emp.getProjects().size();
		System.out.println("Projects: \t" + noOfProjects);
		int noOfActivities = emp.getActivities().size();
		System.out.println("Activities: \t" + noOfActivities);

		// print activities
		if (noOfActivities > 0) {
			this.viewAllActivities(employeeID);
		}
		
		// print projects
		if (noOfProjects > 0) {
			this.viewAllProjects(employeeID);
		}
	}
	
	// show help for a specific (or for all) command
	// @author Asbjørn
	public void help(String command) {
		if (command.equals("view-all")) {
			printArray(Help.viewAll);
		} else if (command.equals("view-week")) {
			printArray(Help.viewWeek);
		} else if (command.equals("project")) {
			printArray(Help.project);
		} else if (command.equals("activity")) {
			printArray(Help.activity);
		} else if (command.equals("employee")) {
			printArray(Help.employee);
		} else {
			// print all
			for (String[] helpSection : Help.all) {
				printArray(helpSection);
			}
		}
	}
	
} // class
