package dtu.projecttracker.app;

// java imports
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.lang.System;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/*
 * This class encapsulates all the objects that make up the model
 * in a MVC design pattern.
 * 
 * @author Asbjørn
 */

public class Model {
	// other app objects
	private App app;

	// calendar utility
	Calendar now = Calendar.getInstance();
	DateFormat format = new SimpleDateFormat("y-MM-dd", Locale.ENGLISH);

	// login fields
	private boolean loginStatus = false;
	private Employee loggedInUser;

	// encapsulated objects
	private ArrayList<Employee> employees;
	private ArrayList<Project> projects;
	private ArrayList<Activity> activities;

	// counters for generating serials
	private int latestProjectSerial = 0; // only the serial - no year #
	private int latestActivitySerial = 0; // unique ascending integers

	private String error; // error-message

	// constructor
	//@David
	public Model(App app) {
		this.app = app;

		// init lists
		employees = new ArrayList<Employee>();
		projects = new ArrayList<Project>();
		activities = new ArrayList<Activity>();
		
		// makes date-format correct (13th month throws exception)
		format.setLenient(false);
	}

	/*
	 * Uncategorized methods
	 */

	// log in with a string from the user
	// @author Asbjørn
	public boolean logIn(String employeeID) throws OperationNotAllowedException {
		employeeID = employeeID.toUpperCase();
		// if the user exists, log them in
		if (hasEmployee(employeeID)) { // 1 (white box)
			setLogInStatus(true);
			setLoggedInUser(getEmployee(employeeID));
		}
		// if the login-string is wrong, set error
		else {
			setError("Login credentials incorrect"); // 2 (white box)
		}
		return loginStatus;
	}

	// register hours on an activity for the currently logged in user
	// @author Deina
	public void registerHours(String activityID, String hours) throws Exception {
		this.getActivity(activityID).registerTimeSpent(hours, this.getLoggedInUser().getID());
	}

	/*
	 * HAS
	 */

	// @return true if an employee with the given ID exists, false otherwise.
	// @author Deina
	public boolean hasEmployee(String employeeID) {
		employeeID = employeeID.toUpperCase();
		for (Employee employee : employees) {
			if (employee.getID().equals(employeeID)) {
				return true;
			}
		}

		// if the code reaches this point, the employee doesn't exist
		return false;
	}

	// converts the string to a date and returns it
	// @author Christian   
	public Date parseString(String date) throws OperationNotAllowedException {
		try {
			return format.parse(date);
		}
		// if the string is not a date, throw exception
		catch (ParseException e) {
			throw new OperationNotAllowedException(this, "You didn't input a correct date");
		}
	}

	// converts the string to a date and returns it
	// @author Christian
	public int parseNum(String number) throws OperationNotAllowedException {
		number = number.replaceAll(",", ".");
		// floors the float and gets the int
		try {
			return (int) Double.parseDouble(number.replaceAll("[^\\d.]", ""));
		} catch (NumberFormatException e) {
			throw new OperationNotAllowedException(this, "Your input was incorrect");
		}
	}

	// @return true if an activity with the given ID exists, false otherwise.
	// @author Christian
	public boolean hasActivity(String activityID) throws Exception {
		for (Activity activity : this.getActivities()) {
			// checks if the activity's ID's match
			if (activity.getID().equals(activityID)) {
				return true;
			}
		}
		// if this point is reached
		// the activity does not exist
		return false;
	}

	// @return true if a project with the given ID exists, false otherwise.
	// @author David
	public boolean hasProject(String projectID) {
		for (Project project : projects) {
			if (project.getID().equals(projectID)) {
				return true;
			}
		}
		return false;
	} // projectWithIDExists

	/*
	 * Create methods
	 */

	// Creates an employee and returns it
	// @author Deina
	public Employee createEmployee(String employeeID) throws Exception {
		if (employeeID.length() == 4) {
			// if the employeeID only contains letters (no special characters or numbers),
			// continue
			if (employeeID.matches("[a-zA-Z]+")) {
				// checks if employee with that name exists
				if (!this.hasEmployee(employeeID)) {
					return new Employee(app, employeeID);
				}

				// if it already exists, throw exception
				else {
					throw new OperationNotAllowedException(this, "An employee with that ID already exists");
				}
			}

			// else, throw exception
			else {
				String errorMessage = "Your ID contains incorrect characters.";
				throw new OperationNotAllowedException(this, errorMessage);
			}
		}

		// else, throw exception
		else {
			String errorMessage = "Your ID is incorrect length. The length should be 4 characters.";
			throw new OperationNotAllowedException(this, errorMessage);
		}

	}

	// Creates a project and returns it
	// @author David
	public Project createProject() {
		return new Project(app);
	}

	// Creates a project with an ID and returns the project
	// @author David
	public Project createProject(String newID) {
		Project newProject = createProject();
		newProject.setID(newID);
		// this.addProject(newProject);
		return newProject;
	}

	// Creates a project activity on a given project and returns the project
	// activity
	// @author Christian
	public ProjectActivity createProjectActivity(String projectID) throws Exception {
		// DbC
		assert projectID != null && projectID.length() == 11 : "preconditions violated";
		int prei = this.getActivities().size();
		// if project does not exist
		if (!hasProject(projectID)) {
			// set error message and throw exception
			String errorMessage = "The project doesn’t exist";
			this.setError(errorMessage);
			throw new Exception(errorMessage);
		}
		// project exists
		else {
			// get project
			Project project = this.getProject(projectID);
			ProjectActivity act = project.createProjectActivity();
			// DbC
			
			assert project.getActivities().contains(act) && this.getActivities().size() == prei+1: "postconditions violated";
			
			return act;
		}
	}

	// Creates a personal activity for a given employee and returns the project
	// activity
	// @author Christian
	public PersonalActivity createPersonalActivity(String employeeID) throws Exception {
		// if employee does not exist
		if (!this.hasEmployee(employeeID)) {
			// set error message and throw exception
			String errorMessage = "The employee doesn't exist";
			this.setError(errorMessage);
			throw new Exception(errorMessage);
		}

		// employee exists
		else {
			// get employee
			Employee emp = this.getEmployee(employeeID);
			return emp.createActivity();
		}
	}

	/*
	 * Complex getters
	 */
	// @return Date object representing now
	// @author Deina
	public Date getToday() {
		return this.now.getTime();
	}

	// @return Date object 24 hours from now
	// @author Deina
	public Date getTomorrow() {
		long tomorrowMillis = System.currentTimeMillis() + (24 * 60 * 60 * 1000);
		Date tomorrow = new Date(tomorrowMillis);
		return tomorrow;
	}

	// @return the project object that matches the given ID String.
	// @author David
	public Project getProject(String projectID) throws OperationNotAllowedException {
		for (Project project : projects) {
			if (project.getID().equals(projectID)) {
				return project;
			}
		}

		// if this point is reached,
		// a project with the ID does not exist
		// setError("Project doesn’t exist");
		throw new OperationNotAllowedException(this, "Project doesn’t exist");
	} // getProjectWithID

	// @return employee with the given employeeID
	// @author Asbjørn
	public Employee getEmployee(String employeeID) throws OperationNotAllowedException {
		employeeID = employeeID.toUpperCase();
		for (Employee employee : employees) {
			if (employee.getID().equals(employeeID)) {
				return employee;
			}
		}

		// if this point is reached, the employee doesn't exist
		throw new OperationNotAllowedException(this, "Employee doesn’t exist");

	}

	// @return the activity object that matches the given ID String.
	// @author Christian
	public Activity getActivity(String activityID) throws Exception {
		// checks if the user is logged in
		if (getLoginStatus()) {

			// goes through all activities and returns one with matching ID
			for (Activity activity : this.activities) {
				if (activity.getID().equals(activityID)) {
					return activity;
				}
			}
			// if this point is reached
			// the activity does not exist
			throw new OperationNotAllowedException(this, "The activity does not exist");
		}
		// else throw exception
		else {
			throw new OperationNotAllowedException(this, "You’re not logged in");
		}
	}

	// Increment latestProjectSerial by one,
	// @return the latest project serial, zero-padded to six digits.
	// @author Asbj�rn
	public String getNextProjectSerial() {
		latestProjectSerial++;
		return String.format("%06d", latestProjectSerial);
	}

	// Increment latestActivitySerial by one, and
	// @return the latest activity serial number zero-padded to six digits.
	// @author David
	public String getNextActivitySerial() {
		latestActivitySerial++;
		return String.format("%06d", latestActivitySerial);
	}

	// returns the error message
	public String getError() {
		return error;
	}

	/*
	 * ASSIGN methods
	 */

	// assign employee to an activity
	// @author David
	public void assignEmployeeToActivity(String activityID, String employeeID) throws Exception {
		Employee emp = this.getEmployee(employeeID.toUpperCase());
		this.getActivity(activityID).assignEmployee(emp);
	}

	// assign employee with ID as the project leader for a project with a specific
	// ID
	// @author David
	public void assignProjectLeader(String projectID, String employeeID) throws Exception {
		// DbC
		assert projectID != null && projectID.length() == 11 && employeeID != null
				&& employeeID.length() == 4 : "Precondition violated";
		// get project
		if (hasProject(projectID)) {
			Project project = getProject(projectID);
			project.assignProjectLeader(employeeID);
			// DbC
			assert project.getProjectLeader().getID().equals(employeeID) : "Postcondition violates";
		}
		// if project doesnt exist
		else {
			throw new OperationNotAllowedException(this, "Project doesn’t exist");
		}
	}

	// assign employee to project
	// @author Deina
	public void assignEmployeeToProject(String projectID, String employeeID) throws Exception {
		employeeID = employeeID.toUpperCase();
		Project project = this.getProject(projectID);
		project.assignEmployee(employeeID);
	}

	/*
	 * 
	 * Simple getter methods.
	 * 
	 */
	
	//@David
	public ArrayList<Project> getProjects() {
		return this.projects;
	}

	public ArrayList<Activity> getActivities() {
		return this.activities;
	}

	public boolean getLoginStatus() {
		return loginStatus;
	}

	public Employee getLoggedInUser() {
		return loggedInUser;
	}

	public ArrayList<Employee> getEmployees() {
		return employees;
	}

	/*
	 * 
	 * Simple setter methods.
	 * 
	 */
	// @author Asbjørn
	public void setLoggedInUser(Employee newLogin) {
		this.loggedInUser = newLogin;
	}

	public void setError(String error) {
		this.error = error;
	}

	// @author Asbjørn
	public void setLogInStatus(boolean loginStatus) {
		this.loginStatus = loginStatus;
	}

	public void addEmployee(Employee employee) {
		employees.add(employee);
	}

	public void addProject(Project project) {
		projects.add(project);
	}

	public void addActivity(Activity activity) {
		activities.add(activity);
	}

	// project setters
	// @author Asbjørn
	public void setProjectStartDate(String projectID, String startDate) throws Exception {
		getProject(projectID).setStartDate(startDate);
	}

	// @author Asbjørn
	public void setProjectEndDate(String projectID, String endDate) throws Exception {
		getProject(projectID).setEndDate(endDate);
	}

	// @author Asbjørn
	public void setProjectBudgetedTime(String projectID, String budgetedTime) throws OperationNotAllowedException {
		getProject(projectID).setBudgetedTime(budgetedTime);
	}

	// @author Asbjørn
	public void setProjectName(String projectID, String name) throws Exception {
		getProject(projectID).setName(name);
	}

	// activity setters
	// @author Asbjørn
	public void setActivityStartDate(String activityID, String startDate) throws Exception {
		this.getActivity(activityID).setStartDate(startDate);
	}

	// @author Asbjørn
	public void setActivityEndDate(String activityID, String endDate) throws Exception {
		this.getActivity(activityID).setEndDate(endDate);
	}

	// @author Asbjørn
	public void setActivityBudgetedTime(String activityID, String budgetedTime) throws Exception {
		this.getActivity(activityID).setBudgetedTime(budgetedTime);
	}

	// @author Asbjørn
	public void setActivityName(String activityID, String name) throws Exception {
		this.getActivity(activityID).setName(name);
	}

} // class
