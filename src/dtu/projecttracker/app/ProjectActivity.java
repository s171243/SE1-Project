package dtu.projecttracker.app;

// java imports
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;

// @author 
public class ProjectActivity extends Activity {
	// encapsulated objects
	private Project project;

	public ProjectActivity(App app) {
		super(app);
	}

	/*
	 * Simple getters
	 */
	public Project getProject() {
		return project;
	}

	/*
	 * Simple setters
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/*
	 * Complex setters
	 */

	// @author Deina
	public void setName(String activityName) throws OperationNotAllowedException {
		// if the logged in user is the projectLeader of the current project,
		// or the current user is on this activity
		if (project.getProjectLeader() == model.getLoggedInUser()) {
			this.activityName = activityName;
		}

		// else, throw exception
		else {
			throw new OperationNotAllowedException(model, "You’re not allowed to change the name of this activity");
		}

	}
} // class
