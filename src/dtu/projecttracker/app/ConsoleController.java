package dtu.projecttracker.app;

import java.util.Arrays;
// java imports
import java.util.Scanner;

import javax.xml.ws.Service.Mode;

import java.lang.Exception;

// @author Asbjørn
public class ConsoleController implements Runnable {
	// other app objects
	private App app;
	private Model model;
	private ConsoleView viewer;

	private Scanner scanner;
	private boolean running;
	private String[] complexCommands = {"project",
										"activity",
										"employee"};
	
	// constructor
	// @author Asbjørn
	public ConsoleController(App app) {
		this.app = app;
		this.model = app.getModel();
		this.viewer = app.getViewer();
		
		// init scanner
		scanner = new Scanner(System.in);
		
		// enable threadloop
		this.running = true;
	}
	
	/*
	 * This is the thread loop. Infinitely reads new
	 * commands and sends them to the interpreter.
	 * @see java.lang.Runnable#run()
	 * @author Asbjørn
	 */
	public void run() {
		// login before doing anything else
		login();
		
		// then start the main loop
		while (this.running) {
			viewer.commandPrompt();
			String command = scanner.nextLine();
			// strip "s, force lowercase
			command = command.toLowerCase().replaceAll("\"", "");
			interpretCommand(command);
		}
	} // run

	/*
	 * Reads a four-character username, and calls login method on model.
	 * @author Asbjørn
	 */
	public void login() {
		boolean loginSuccess;
		do {
			viewer.loginPrompt();
			String username = scanner.nextLine();

			try {
				loginSuccess = model.logIn(username);
			} catch (Exception ex) {
				loginSuccess = false;
				viewer.loginFailure();
			}
		} while (!loginSuccess);
		viewer.loginSuccess();
	}
	
	/* 
	 * This method handles parsing the command.
	 * @author Asbjørn
	 */
	private void interpretCommand(String command) {
		// split command string by spaces
		String[] commandArray = command.split(" ");
		String commandName = commandArray[0].toLowerCase();

		// parse complex commands
		boolean complexCommand = Arrays.asList(complexCommands).contains(commandName);
		if (complexCommand && commandArray.length >= 3) {
			
			// id of the object to manipulate
			String ID = commandArray[1].toLowerCase();
			
			// the action to perform on the object
			String action = commandArray[2].toLowerCase();

			// any additional arguments (re-join by spaces)
			String argument = null;
			if (commandArray.length > 3) {
				argument = String.join(" ", Arrays.asList(commandArray).subList(3, commandArray.length));
			}
			
			// PROJECT
			if (commandName.equals("project")) {
				projectCommand(ID, action, argument);

			// ACTIVITY
			} else if (commandName.equals("activity")) {
				activityCommand(ID, action, argument);
				
			// EMPLOYEE
			} else if (commandName.equals("employee")) {
				employeeCommand(ID, action, argument);
			}
			
		} else if (complexCommand && commandArray.length < 3) {
			viewer.badSyntax();
			
		// VIEW-ALL
		} else if (commandName.equals("view-all")) {
			if (commandArray.length == 2) {
				this.viewAllCommand(commandArray[1]);
			} else {
				viewer.badSyntax();
			}

		// VIEW-WEEK
		} else if (commandName.equals("view-week")) {
			if (commandArray.length == 2) {
				viewer.viewWeek(commandArray[1]);
			} else {
				viewer.badSyntax();
			}

		// QUIT
		} else if (commandName.equals("quit")) {
			this.quit();
			
		// HELP
		} else if (commandName.equals("help")) {
			if (commandArray.length == 2) {
				viewer.help(commandArray[1]);
			} else if (commandArray.length == 1) {
				viewer.help("");
			} else {
				viewer.badSyntax();
			}

		// NEW-PROJECT
		} else if (commandName.equals("new-project")) {
			newProject();
			
		// NEW-EMPLOYEE
		} else if (commandName.equals("new-employee")) {
			if (commandArray.length == 2) {
				newEmployee(commandArray[1].toUpperCase());
			} else {
				viewer.badSyntax();
			}
			
		// COMMAND NOT FOUND
		} else {
			viewer.commandNotFound();
		}
	} // interpretCommand
	
	/*
	 * VIEW-ALL
	 */
	// @author Asbjørn
	private void viewAllCommand(String type) {
		if (type.equals("projects")) {
			try {
				viewer.viewAllProjects("");
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
		} else if (type.equals("employees")) {
			viewer.viewAllEmployees();
		} else if (type.equals("activities")) {
			try {
				viewer.viewAllActivities(""); // blank string to show ALL
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
		} else {
			viewer.badSyntax();
		}
	}
	
	/*
	 * PROJECT
	 */
	// @author Asbjørn
	private void projectCommand(String projectID, String action, String argument) {
		// view all info on project
		if (action.equals("view")) {
			try {
				viewer.viewProject(projectID);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// make new project activity
		} else if (action.equals("new-activity")) {
			try {
				String actID = model.createProjectActivity(projectID).getID();
				viewer.createdActivity(actID); //*/
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// assign employee to project
		} else if (action.equals("assign-employee")) {
			try {
				model.assignEmployeeToProject(projectID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// set projectleader
		} else if (action.equals("set-projectleader")) {
			try {
				model.assignProjectLeader(projectID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}

		// set project start date
		} else if (action.equals("set-start-date")) {
			try {
				model.setProjectStartDate(projectID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// set project end date
		} else if (action.equals("set-end-date")) {
			try {
				model.setProjectEndDate(projectID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// set project budgeted time
		} else if (action.equals("set-budgeted-time")) {
			try { 
				model.setProjectBudgetedTime(projectID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}

		// set project name
		} else if (action.equals("set-name")) {
			try {
				model.setProjectName(projectID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
		} else {
			viewer.badSyntax();
		}
	} // projectCommand

	/*
	 * ACTIVITY
	 */
	// @author Asbjørn
	private void activityCommand(String activityID, String action, String argument) {
		// view information about activity
		if (action.equals("view")) {
			try {
				viewer.viewActivity(activityID);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// register hours spent on activity, as logged in user
		} else if (action.equals("register-hours")) {
			try {
				model.registerHours(activityID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}

		// add employee to an activity by employee ID
		} else if (action.equals("assign-employee")) {
			try {
				model.assignEmployeeToActivity(activityID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}

		// set activity start date
		} else if (action.equals("set-start-date")) {
			try {
				model.setActivityStartDate(activityID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// set activity end date
		} else if (action.equals("set-end-date")) {
			try {
				model.setActivityEndDate(activityID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// set activity budgeted time
		} else if (action.equals("set-budgeted-time")) {
			try {
				model.setActivityBudgetedTime(activityID, argument);
	 		} catch (Exception ex) {
	 			viewer.genericError(model.getError());
	 		}
			
		// set activity name
		} else if (action.equals("set-name")) {
			try {
				model.setActivityName(activityID, argument);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
			
		// in case of bad syntax
		} else {
			viewer.badSyntax();
		}
	} // activityCommand

	/*
	 * EMPLOYEE
	 */
	// @author Deina @ David
	private void employeeCommand(String employeeID, String action, String argument) {
		// case correct the ID
		employeeID = employeeID.toUpperCase();
		
		// view employee info
		if (action.equals("view")) {
			try {
				viewer.viewEmployee(employeeID);
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}

		// add personal activity to employee
		} else if (action.equals("new-activity")) {
			try {
				PersonalActivity act = model.createPersonalActivity(employeeID);
				viewer.createdActivity(act.getID());
			} catch (Exception ex) {
				viewer.genericError(model.getError());
			}
		}
	} // employeeCommand
	
	/*
	 * NEW-PROJECT
	 */
	// @author David
	public void newProject() {
		Project proj = model.createProject();
		viewer.createdProject(proj.getID());
	}
	
	/*
	 * NEW-EMPLOYEE
	 */
	// @author Deina
	public void newEmployee(String employeeID) {
		try {
			model.createEmployee(employeeID);
		} catch (Exception ex) {
			viewer.genericError(model.getError());
		}
	}
	
	/*
	 * QUIT
	 */
	// @author Asbjørn
	private void quit() {
		this.setRunning(false);
		viewer.terminate();
	}
	
	/*
	 * Simple getters
	 */
	// @author Asbjørn
	public boolean getRunning() {
		return this.running;
	}

	/*
	 * Simple setters
	 */
	// @author Asbjørn
	public void setRunning(boolean running) {
		this.running = running;
	}
} // class
