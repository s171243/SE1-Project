package dtu.projecttracker.app;

public class Help {
	public final static String[] basic = {
		"|--------------------|",
		"|-- BASIC COMMANDS --|",
		"|--------------------|",
		"help <command>",
		"\tLists help for a command, or all commands if none are specified.",
		"",
		"quit",
		"\tTerminate the program.",
		"",
		"|-----------------------|",
		"|-- NEW BASIC OBJECTS --|",
		"|-----------------------|",
		"new-project",
		"\tCreates a new project, and prints its project ID.",
		"",
		"new-employee <employee-id>",
		"\tCreates a new employee. ID must be four characters.",
		""};

	public final static String[] viewAll = {
		"|--------------|",
		"|-- VIEW ALL --|",
		"|--------------|",
		"\tView information about all objects of a certain type.",
		"",
		"view-all projects",
		"\tLists all projects.",
		"",
		"view-all employees",
		"\tLists all employees.",
		"",
		"view-all activites",
		"\tLists all activities.",
		""
	};
	
	public final static String[] viewWeek = {
		"|---------------|",
		"|-- VIEW WEEK --|",
		"|---------------|",
		"\tView information about employees and their activities in a given week.",
		"",
		"view-week <YYYY-MM-DD>",
		"\tList all employees, and the amount of activities they have scheduled for the week containing the given date.",
		""
	};
	

	public final static String[] project = {
		"|-------------|",
		"|-- PROJECT --|",
		"|-------------|",
		"\tManipulate and view informatoun about projects.",
		"",
		"project <project-id> view",
		"\tView all information about the project.",
		"",
		"project <project-id> new-activity",
		"\tAdd a new activity to the project, and print the ID that it gets.",
		"",
		"project <project-id> assign-employee <employee-id>",
		"\tAssign an employee to the project.",
		"",
		"project <project-id> set-projectleader <employee-id>",
		"\tSet the project leader for a project.",
		"",
		"project <project-id> set-start-date <YYYY-MM-DD>",
		"\tSet start date of the project.",
		"",
		"project <project-id> set-end-date <YYYY-MM-DD>",
		"\tSet end date of the project.",
		"",
		"project <project-id> set-budgeted-time <hours>",
		"\tSet budgeted time for the project.",
		"",
		"project <project-id> set-name <string>",
		"\tSet a (human-readable) name for the project.",
		""
	};

	public final static String[] activity = {
		"|--------------|",
		"|-- ACTIVITY --|",
		"|--------------|",
		"\tManipulate and view information about activities.",
		"",
		"activity <activity-id> view",
		"\tView all information about an activity.",
		"",
		"activity <activity-id> register-hours <hours>",
		"\tRegister a number of hours spent. The hours are logged as the currently logged in user.",
		"",
		"activity <activity-id> assign-employee <employee-id>",
		"\tAssign an employee to a given activity.",
		"",
		"activity <activity-id> set-start-date <YYYY-MM-DD>",
		"\tSets start date for an activity.",
		"",
		"activity <activity-id> set-end-date <YYYY-MM-DD>",
		"\tSets end date for an activity.",
		"",
		"activity <activity-id> set-budgeted-time <hours>",
		"\tSets budgeted time for the activity.",
		"",
		"activity <activity-id> set-name <string>",
		"\tSet the (human-readable) name of the activity.",
		""
	};

	public final static String[] employee = {
		"|--------------|",
		"|-- EMPLOYEE --|",
		"|--------------|",
		"\tManipulate and view information about activities.",
		"",
		"employee <employee-id> view",
		"\tView all information about an employee.",
		"",
		"employee <employee-id> new-activity",
		"\tCreate a new personal activity, and print the ID that it gets.",
		""
	};
	
	public final static String[][] all = {
			basic,
			viewAll,
			viewWeek,
			project,
			activity,
			employee
	};
}
