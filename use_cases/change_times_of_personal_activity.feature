Feature: Change times of personal activity
	Description: Change the start time, end time and budgeted time for personal activity
	Actor: User
	
Background:
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the personal activity with ID "000200" exists
	
 Scenario: Change the start time of an activity "000200" to "2018-10-12"
	Given the start date of the activity is "2018-03-12"
	Given the end date of the activity is "2019-10-12"
	When user tries to change the activity start date to "2018-10-12"
	Then the start date of activity with ID "000200" is "2018-10-12"
	
Scenario: Change the end time of an activity "000200" to "2018-12-12"
	Given the end date of the activity is "2019-10-12"
	And the date "2019-10-14" is after the end date of the activity
	When a user tries to change the activity end date to "2018-12-12"
	Then the end date of the activity with ID "000200" is "2018-12-12"
	
Scenario: Change the end time of an activity "000200" to a date before the start time
	Given the start date of the activity is "2018-03-12"
	And the date "2017-01-01" is before the start time of the activity
	When a user tries to change the activity end date to "2017-01-01"
	Then show error "The end date is before the start date"
	
Scenario: Change the budgeted time of an activity "000200" to "20"
	When a user tries to change the budgeted time to "20"
	Then the budgeted time of the activity with ID "000200" is 20
	
