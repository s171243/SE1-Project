Feature: Add an employee to a project
	Description: Add employee to a project
	Actors: User

Background:
	Given the project with ID "2018-000100" exists
	And an employee with ID "ABCD" exists
	
Scenario: Add an employee to a project
	Given the user is logged in with initials "ABCD"
	And an employee with ID "ADDD" exists
	And the employee with initials "ABCD" is projectLeader for the project
	And the employee with initials "ADDD" isn’t already added to the project
	When the employee is added to the project
	And the user asks for the list of employees on the project
	Then the user finds employees with IDs "ABCD" and "ADDD" on the project with ID
	
Scenario: Add an employee to a project
	#Given another employee with ID "ADDD" exists
	Given the user is logged in with initials "ABCD"
	Given an employee with ID "ADDD" exists
	And the employee with initials "ABCD" is projectLeader for the project
	And the employee with initials "ADDD" isn’t already added to the project
	When the employee is added to the project
	Then the employee with initials "ADDD" is added to the project with ID "2018-000100"
	
Scenario: Add an already assigned employee to a project
	Given an employee with ID "ADDD" exists
	Given the user is logged in with initials "ABCD"
	And the employee with initials "ABCD" is projectLeader for the project
	And the employee with initials "ADDD" is already added to the project
	When the employee is added to the project
	Then show error "The employee is already added to the project"
	
Scenario: Add an non-existing employee to a project
	Given the employee with initials "ADDD" isn’t already added to the project
	And an employee with ID "ADDD" does not exist
	When the employee is added to the project
	Then show error "The employee doesn’t exist"
	
Scenario: Add an employee to a project
	Given the user is logged in with initials "ABCD"
	Given an employee with ID "ADDD" exists
	And the projectLeader for the project has ID "PLPL"
	And the employee with initials "ABCD" is not projectLeader for the project
	And the employee with initials "ADDD" isn’t already added to the project
	When the employee is added to the project
	Then show error "You’re not a project leader and can’t add users"
	
Scenario: Add an already assigned employee to a project
	Given an employee with ID "ADDD" exists
	Given the projectLeader for the project has ID "PLPL"
	Given the user is logged in with initials "ABCD"
	Given the employee with initials "ABCD" is not projectLeader for the project
	And the employee with initials "ADDD" is already added to the project
	When the employee is added to the project
	Then show error "You’re not a project leader and can’t add users"
	
Scenario: Add an non-existing employee to a project
	Given an employee with ID "ADDD" does not exist
	Given the employee with initials "ADDD" isn’t already added to the project
	Given the user is logged in with initials "ABCD"
	When the employee is added to the project
	Then show error "The employee doesn’t exist"

Scenario: Add an employee to a project
	Given the user is logged in with initials "ABCD"
	And an employee with ID "ADDD" exists
	And the project activity with ID "000100" exists
	When the user asks for the project of the project activity
	Then the user finds the project with ID "2018-000100"