Feature: Create a project
	Description: Project created
	Actor: User
	
Scenario: Create a project
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	When a project is created
	Then the new project exists