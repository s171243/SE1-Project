Feature: Create an activity
	Description: Create activity
	Actor: Employee or ProjectLeader

Scenario: Create Personal Activity
	Given an employee with ID "ADDD" does not exist
	When the user tries to add a personal activity
	Then show error "The employee doesn't exist"

Scenario: Create a personal activity
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the project with ID "2018-000100" exists
	When the user tries to add a personal activity
	Then the employee with initials "ABCD" has an activity
	
Scenario: Create an activity on project "2018-000201"
	Given the project with ID "2018-000100" exists
	And an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the employee with initials "ABCD" is projectLeader for the project
	When the user tries to create a project activity
	Then the project with ID "2018-000100" has an activity

Scenario: Create an activity on project "2018-000201"
	Given the project with ID "2018-000100" exists
	And an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the projectLeader for the project has ID "PLPL"
	And the employee with initials "ABCD" is not projectLeader for the project
	When the user tries to create a project activity
	Then show error "You’re not allowed to create an activity on this project"
	
Scenario: Create activity
	Given the project with ID "2018-000100" exists
	And an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the employee with initials "ABCD" is projectLeader for the project
	When the user tries to create a project activity with ID "000100"
	Then the project activity with ID "000100" now exists
	
Scenario: Create an activity on project "2018-000201"
	Given the project with ID "2018-000201" doesn't exist
	When the user tries to create a project activity
	Then show error "The project doesn’t exist"