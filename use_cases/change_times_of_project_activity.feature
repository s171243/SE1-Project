Feature: Change times of project activity
	Description: Change the start time, end time and budgeted time for project activity
	Actor: User
	
Background:
	Given the project with ID "2018-000100" exists
	Given the project activity with ID "000100" exists
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the employee with initials "ABCD" is projectLeader for the project
	
Scenario: Change the start time of activity "000101" to "2018-10-12"
	Given the end date of the activity is "2019-10-12"
	When user tries to change the project activity start date to "2018-10-12"
	Then the start date of project activity with ID "000100" is "2018-10-12"
	
Scenario: Change the end time of a project activity "000101" to "2018-12-12"
	When a user tries to change the end date of the activity to "2018-12-12"
	Then the end date of the activity with ID "000100" is "2018-12-12"
	
Scenario: Change the end time of a project activity "000101" to a date before the start time
	Given the start date of the project activity is "2018-10-12"
	And the date "2017-01-01" is before the start time of the activity
	When a user tries to change the end date of the activity to "2017-01-01"
	Then show error "The end date is before the start date"
	
Scenario: Change the budgeted time of a project activity "000101" to "20"
	When a user tries to change the budgeted time to "20"
	Then the budgeted time of the activity with ID "000100" is 20
	
Scenario: Change time of activity with incorrect string
	When user tries to change the project activity start date to "2018-10-xx"
	Then show error "You didn't input a correct date"
	
Scenario: Add time to activity
	Given the employee with ID "ABCD" is assigned to project activity with ID "000100" 
	And the employee has already added "5" hours to the activity
	When the user tries to register "5" hours on an activity
	Then 10 hours is added to the activity