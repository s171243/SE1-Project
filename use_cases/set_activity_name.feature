Feature: Set activity name
	Description: Set a name of a activity
	Actor: Users
	
# Senarier hvor brugeren er logged in og aktiviteten eksistere

# 
Scenario: Set project activity name with ID: "000100" to: "Test"
	Given an employee with ID "ABCD" exists
	Given the user is logged in with initials "ABCD"
	Given the project with ID "2018-000100" exists
	And the employee with initials "ABCD" is projectLeader for the project
	And the project activity with ID "000100" exists
	When the user renames the project activity to "Test"
	Then the name of the activity with ID "000100" is "Test"
	
Scenario: Set project activity name with ID: "000100" to: "Test"
	Given an employee with ID "ABCD" exists
	Given the user is logged in with initials "ABCD"
	Given the project with ID "2018-000100" exists
	Given the project activity with ID "000100" exists
	And the projectLeader for the project has ID "PLPL"
	And the employee with initials "ABCD" is not projectLeader for the project
	When the user renames the project activity to "Test"
	Then show error "You’re not allowed to change the name of this activity"
	
Scenario: Set project activity name with ID: "000100" to: "Test"
	Given an employee with ID "ABCD" exists
	Given the user is logged in with initials "ABCD"
	Given the project with ID "2018-000100" exists
	Given the project activity with ID "000100" exists
	And the projectLeader for the project has ID "PLPL"
	And the employee with initials "ABCD" is not projectLeader for the project
	When the user renames the project activity to "Test"
	Then show error "You’re not allowed to change the name of this activity"

Scenario: Set personal activity name with ID "000100" to: "Vacation"
	Given an employee with ID "ABCD" exists
	Given the user is logged in with initials "ABCD"
	Given the project with ID "2018-000100" exists
	Given the personal activity with ID "000200" exists
	And the user is logged in with initials "ABCD"
	And the employee with ID "ABCD" is assigned to personal activity with ID "000200"
	When the user changes the name of the activity to "Vacation"
	Then the name of the personal activity with ID "000200" is "Vacation"
	
Scenario: Set personal activity name of another users personal activity
	Given an employee with ID "ABCD" exists
	Given the user is logged in with initials "ABCD"
	Given the project with ID "2018-000100" exists
	Given the personal activity with ID "000200" exists
	And an employee with ID "ABBB" exists
	And the user is logged in with initials "ABBB"
	And the employee with initials "ABCD" is not the owner of the personal activity
	When the user changes the name of the activity to "Vacation"
	Then show error "You’re not allowed to change the name of this activity"
	
# Senarier hvor brugeren ikke er logged in og aktiviteten eksistere

#
Scenario: Non logged in user tries to set project activity name with ID: "000100" to: "Test"
	Given an employee with ID "ABCD" exists
	Given the project with ID "2018-000100" exists
	And the project activity with ID "000100" exists
	And that the user is not logged in
	When the user renames the project activity to "Test"
	Then show error "You’re not logged in"
	
Scenario: Non logged in user tries to set personal activity name with ID "000100" to: "Vacation"
	Given an employee with ID "ABCD" exists
	Given the personal activity with ID "000200" exists
	And that the user is not logged in
	When the user changes the name of the activity to "Vacation"
	Then show error "You’re not logged in"

# Senarier hvor brugeren er logged in og aktiviteten ikke eksistere

#
Scenario: Set project activity name with ID: "000101" to: "Test"
	Given an employee with ID "ABCD" exists
	Given the user is logged in with initials "ABCD"
	Given the project with ID "2018-000100" exists	
	Given the project activity with ID "000101" doesn't exist
	When the user renames the project activity to "Test"
	Then show error "The activity does not exist"

Scenario: Set personal activity name with ID "000101" to: "Vacation"
	Given an employee with ID "ABCD" exists
	Given the personal activity with ID "000201" doesn't exist
	And the user is logged in with initials "ABCD"
	When the user changes the name of the activity to "Vacation"
	Then show error "The activity does not exist"
	
# Senarier hvor brugeren ikke er logged in og aktiviteten ikke eksistere

#
Scenario: Set project activity name with ID: "000101" to: "Test"
	Given the project activity with ID "000101" doesn't exist
	And that the user is not logged in
	When the user renames the project activity to "Test"
	Then show error "You’re not logged in"


Scenario: Set personal activity name with ID "000101" to: "Vacation"
	Given an employee with ID "ABCD" exists
	Given the personal activity with ID "000201" doesn't exist
	And that the user is not logged in
	When the user changes the name of the activity to "Vacation"
	Then show error "You’re not logged in"

