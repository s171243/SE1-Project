Feature: Change times of project
	Description: Change the time of a project
	Actor: Project leader
	
Background:
	Given the project with ID "2018-000100" exists
	And an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the employee with initials "ABCD" is projectLeader for the project

Scenario: Get total spent time on a project
	And the project activity with ID "000100" exists
	And the employee with ID "ABCD" is assigned to project activity with ID "000100"
	And the employee has already added "5" hours to the activity
	When the user tries to register "5" hours on an activity
	Then the total spent time of the project with ID "2018-000100" is 10 hours

Scenario: Change the start time of a project "2018-000100" to "2018-10-12"
	Given the end date of the project is "2019-10-12"
	When a user tries to change the project start date to "2018-10-12"
	Then the start date of project with ID "2018-000100" is "2018-10-12"
	
Scenario: Change the end time of a project "2018-000100" to "2018-12-12"
	Given the start date of the project is "2017-10-12"
	When a user tries to change the project end date to "2018-12-12"
	Then the end date of project with ID "2018-000100" is "2018-12-12"
	
Scenario: Change the end time of a project "2018-000100" to a date before the start time
	Given the start date of the project is "2017-10-12"
	And the date "2017-01-01" is before the start time
	When a user tries to change the project end date to "2017-01-01"
	Then show error "The end date is before the start date"

Scenario: Change the start time of a project "2018-000100" to a date after the end time
	Given the end date of the project is "2019-10-12"
	When a user tries to change the project start date to "2019-12-12"
	Then show error "The start date is after the end date"
	
Scenario: Change the budgeted time of a project "2018-000100" to "100"
	Given the budgeted time of project "2018-000100" is "0"
	When a user tries to change budgeted time to "100"
	Then the budgeted time of project with ID "2018-000100" is "100"
