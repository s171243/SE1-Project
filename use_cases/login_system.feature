Feature: Login_system
	Description: Logs you in
	Actor: Users

Background:
	Given that the user is not logged in
	And an employee with ID "ABCD" exists

Scenario: User can login
	When user enters "ABCD"
	Then user login succeeds
	And user is logged in, using "ABCD" as credentials
	
Scenario: User can login using lower case initials
	When user enters lower case "abcd"
	Then user is logged in, using "ABCD" as credentials

Scenario: User logs in with wrong initials
	Given an employee with ID "ABBB" does not exist
	When user enters "ABBB"
	Then user is not logged in
	And show error "Login credentials incorrect"
