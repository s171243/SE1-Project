Feature: Add an employee to an activity
	Description: Employee added to activity
	Actors: ProjectLeader
	
Background:
	Given the project with ID "2018-000100" exists
	And the project activity with ID "000100" exists
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the employee with initials "ABCD" is projectLeader for the project

Scenario: Add an employee to an activity
	Given the employee isn’t already added to the activity
	When the user tries to add the employee to the activity
	And the user asks for the list of employees on the activity
	Then the user finds an employee with ID "ABCD"

Scenario: Add an employee to an activity
	Given the employee isn’t already added to the activity
	When the user tries to add the employee to the activity
	Then the employee with initials "ABCD" is added to the activity with ID "000100"
	
Scenario: Add an already assigned employee to an activity
	Given the employee with ID "ABCD" is assigned to project activity with ID "000100"
	When the user tries to add the employee to the activity
	Then show error "Employee already assigned to this activity"

Scenario: Add a non-existant employee to an activity
	Given the employee isn’t already added to the activity
	And an employee with ID "ADDD" does not exist
	When the user tries to add the employee to the activity
	Then show error "Employee doesn’t exist"

Scenario: Add an employee to an activity
	Given an employee with ID "ADDD" exists
	And the employee with initials "ADDD" is already added to the project
	When the user asks for the list of projects with the employee
	Then the user gets the project with ID "2018-000100"
