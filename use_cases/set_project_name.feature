 Feature: Set project name
	Description: Set the name os a project
	Actor: employee
	
# Senarier hvor brugeren er logged in

# Brugeren er logged in og projektet eksistere
Scenario: Set project name on project "2018-000100" to: "Development project"
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the project with ID "2018-000100" exists
	And the employee with initials "ABCD" is projectLeader for the project
	When the user adds a string of name: "Development project"
	Then the name of project with ID "2018-000100" to "Development project"
	
Scenario: Set project name on project "2018-000100" to: "Development project"
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And an employee with ID "ABBB" exists
	And the project with ID "2018-000100" exists
	And the employee with initials "ABBB" is projectLeader for the project
	When the user adds a string of name: "Development project"
	Then show error "You’re not allowed to change the name of this project"

# Brugeren er logged in og projektet eksistere IKKE
Scenario: Set project name on non-existent project "2018-000101" to: "Development project"
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the project with ID "2018-000101" doesn't exist
	When the user adds a string of name: "Development project"
	Then show error "Project doesn’t exist"
	
# Senarier hvor brugeren IKKE er logged in

# Brugeren er IKKE logged in og projektet eksistere
Scenario: Non logged in user tries to set project name on project "2018-000100" to: "Development project"
	Given the project with ID "2018-000100" exists
	And that the user is not logged in
	When the user adds a string of name: "Development project"
	Then show error "You’re not logged in"

# Brugeren er IKKE logged in og projektet eksistere IKKE
Scenario: Non logged in user tries to set project name on non-existent project "2018-000101" to: "Development project"
	Given the project with ID "2018-000101" doesn't exist
	And that the user is not logged in
	When the user adds a string of name: "Development project"
	Then show error "Project doesn’t exist"