Feature: Temporary file
	Description: Temporary steps
	Actors: ProjectLeader
	
Scenario: Get list of projects
	Given the project with ID "2018-000100" exists
	And the project with ID "2018-000102" exists
	When the user asks for the list of projects
	Then the user finds projects with IDs "2018-000100" and "2018-000102"
	
Scenario: Get list of employees
	Given an employee with ID "ABCD" exists
	Given an employee with ID "ABBB" exists
	When the user asks for the list of employees
	Then the user finds employees with IDs "ABCD" and "ABBB"
	
Scenario: Get whether the activity exists
	Given an employee with ID "ABCD" exists
	And the user is logged in with initials "ABCD"
	And the personal activity with ID "000200" exists
	When the user asks whether the activity exists
	Then the user is told, that the activity exists
	