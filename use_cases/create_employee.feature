Feature: Create an employee
	Description: Create an employee
	Actors: User
	
Scenario: Create user "ABBB"
	Given an employee with ID "ABBB" does not exist
	When an employee is created with ID "ABBB"
	Then the employee with ID "ABBB" exists

Scenario: Create user with lowercase letters "abcd"
	Given an employee with ID "ABBB" does not exist
	When an employee is created with ID "abbb"
	Then an employee with ID "ABBB" exists
	
Scenario: Create user "ABCD" when the ID already exists
	Given an employee with ID "ABCD" exists
	When an employee is created with ID "ABCD"
	Then show error "An employee with that ID already exists"
	
Scenario: Create user with incorrect length
	Given an employee with ID "ABCD" exists
	Given the user is logged in with initials "ABCD"
	When an employee is created with ID "ABC"
	Then show error "Your ID is incorrect length. The length should be 4 characters."
	
Scenario: Create user with incorrect characters
	When an employee is created with ID "ABC$"
	Then show error "Your ID contains incorrect characters."