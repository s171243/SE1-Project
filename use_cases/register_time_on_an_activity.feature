Feature: Register time on an activity 
	Description: Register time for a employee on a activity
	Actor: Employee
	
Background: 
	Given an employee with ID "ABCD" exists 
	Given the user is logged in with initials "ABCD" 
	
Scenario: Employee registers time on an activity with ID "000100" on the project with ID "2018-000201" 
	Given the project with ID "2018-000100" exists 
	Given the project activity with ID "000100" exists 
	And the employee with ID "ABCD" is assigned to project activity with ID "000100" 
	When the user tries to register "5" hours on an activity 
	Then 5 hours is added to the activity 
	
Scenario: Employee registers time on an activity "000100" of the project "2018-000201" using "4AB" 
	Given the project with ID "2018-000100" exists 
	Given the project activity with ID "000100" exists 
	And the employee with ID "ABCD" is assigned to project activity with ID "000100" 
	When the user tries to register "4ab" hours on an activity 
	Then 4 hours is added to the activity 
	
Scenario: Employee registers time on an activity "000100" of the project "2018-000201" using "42,131.3abc" 
	Given the project with ID "2018-000100" exists 
	Given the project activity with ID "000100" exists 
	And the employee with ID "ABCD" is assigned to project activity with ID "000100" 
	When the user tries to register "42,4213fad" hours on an activity 
	Then 42 hours is added to the activity 
	
Scenario: Employee registers time on an activity with ID "000100" on the project with ID "2018-000201" 
	Given the project with ID "2018-000100" exists 
	Given the project activity with ID "000100" exists 
	And the employee with ID "ABCD" is not assigned to activity with ID "000100" 
	When the user tries to register "5" hours on an activity 
	Then show error "You are not assigned to this activity" 
	
Scenario: Employee registers time on an personal activity: "Sickness" 
	Given the personal activity with ID "000200" exists 
	And the employee with ID "ABCD" is assigned to personal activity with ID "000200" 
	When the user tries to register "5" hours on a personal activity 
	Then 5 hours is added to the personal activity 
	
Scenario: Employee registers time on an personal activity: "Sickness" 
	Given an employee with ID "ABBB" exists
	Given the personal activity with ID "000200" exists
	And the employee with ID "ABBB" is assigned to personal activity with ID "000200" 
	And the employee with ID "ABCD" is not assigned to activity with ID "000200"
	When the user tries to register "5" hours on a personal activity 
	Then show error "You are not assigned to this activity" 
	
	
Scenario: Employee registers time on an personal activity: "Sickness" 
	Given the personal activity with ID "000200" exists
	And the employee with ID "ABCD" is assigned to personal activity with ID "000200" 
	When the user tries to register "smfdslkfk" hours on a personal activity 
	Then show error "Your input was incorrect"