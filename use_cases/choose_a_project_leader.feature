Feature: Choose a project leader
	Description: Makes a employee a project leader
	Actor: Employee

Scenario: Choose a new projectLeader for the project "2018-0201"
	Given the project with ID "2018-000100" exists
	And an employee with ID "ABCD" exists
	When the projectLeader is added to the project
	Then the projectLeader with initials "ABCD" is assigned to the project with ID "2018-000100"

Scenario: Choose a new projectLeader for the project "2018-0201"
	Given the project with ID "2018-000101" doesn't exist
	And an employee with ID "ABCD" exists
	When the projectLeader is added to the project
	Then show error "Project doesn’t exist"
	
Scenario: Choose a new projectLeader for the project "2018-0201"
	Given the project with ID "2018-000101" doesn't exist
	And an employee with ID "ADDD" does not exist
	When the projectLeader is added to the project
	Then show error "Project doesn’t exist"
	
Scenario: Choose a new projectLeader for the project "2018-0201"
	Given the project with ID "2018-000100" exists
	And an employee with ID "ADDD" does not exist
	When the projectLeader is added to the project
	Then show error "Employee doesn’t exist"
	
Scenario: Choose a new projectLeader for the project "2018-0201"
	Given the project with ID "2018-000101" doesn't exist
	And an employee with ID "ABCD" exists
	When the projectLeader is added to the project
	Then show error "Project doesn’t exist"
