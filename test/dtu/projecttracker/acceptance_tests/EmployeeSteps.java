package dtu.projecttracker.acceptance_tests;

import static org.junit.Assert.assertEquals;
// junit4 imports
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

// cucumber imports
import cucumber.api.PendingException;
import cucumber.api.java.en.*;
// application layer imports
import dtu.projecttracker.app.App;
import dtu.projecttracker.app.Model;
import dtu.projecttracker.app.Project;
import dtu.projecttracker.app.Employee;

public class EmployeeSteps {
	private App app;
	private Model model;
	private Project project;
	private ArrayList<Employee> employees;
	
	/*
	 * This constructor uses the principle "dependency injection", 
	 * and is only called in the Cucumber libraries.
	 * 
	 */
	public EmployeeSteps(App app) {
		this.app = app;
		this.model = app.getModel();
	}
	//@author David
	@Given("^an employee with ID \"([^\"]*)\" exists$")
	public void anEmployeeWithIDExist(String employeeID) throws Exception {
		model.createEmployee(employeeID);
		app.setEmployeeID(employeeID);
	}
	//@author David
	@Then("^the employee with ID \"([^\"]*)\" exists$")
	public void theEmployeeWithIDExists(String employeeID) throws Exception {
	    assertTrue(model.hasEmployee(employeeID));
	}
	//@author David
	@When("^an employee is created with ID \"([^\"]*)\"$")
	public void anEmployeeIsCreatedWithID(String newEmployeeID) throws Exception {
		try {
		    model.createEmployee(newEmployeeID);
		} catch (Exception e) {
		}
	}

	//@author David
	@Given("^the employee with initials \"([^\"]*)\" is projectLeader for the project$")
	public void theEmployeeWithInitialsIsProjectLeaderForTheProject(String employeeID) throws Exception {
		model.getProject(app.getProjectID()).assignProjectLeader(employeeID);
	}
	//@author David
	@Given("^the employee with initials \"([^\"]*)\" is not projectLeader for the project$")
	public void theEmployeeWithInitialsIsNotProjectLeaderForTheProject(String employeeID) throws Exception {
	    assertNotEquals(model.getProject(app.getProjectID()).getProjectLeader(), model.getEmployee(employeeID));
	}
	//@author Deina
	@Given("^the employee with initials \"([^\"]*)\" is not the owner of the personal activity$")
	public void theEmployeeWithInitialsIsNotTheOwnerOfThePersonalActivity(String employeeID) throws Exception {
		project = model.getProject(app.getProjectID());
		assertNotEquals(project.getProjectLeader(), model.getLoggedInUser());
	}
	//@author Deina
	@When("^the user asks for the list of employees$")
	public void theUserAsksForTheListOfEmployees() throws Exception {
	    employees = model.getEmployees();
	}
	//@author Deina
	@Then("^the user finds employees with IDs \"([^\"]*)\" and \"([^\"]*)\"$")
	public void theUserFindsEmployeesWithIDsAnd(String employeeID1, String employeeID2) throws Exception {
		assertEquals(2, employees.size());
		Employee employee1 = employees.get(0);
		Employee employee2 = employees.get(1);
		assertTrue(employee1.getID().equals(employeeID1) && employee2.getID().equals(employeeID2)
				|| employee1.getID().equals(employeeID2) && employee2.getID().equals(employeeID1));
	}
	
} // class end
