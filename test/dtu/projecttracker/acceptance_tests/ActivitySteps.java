package dtu.projecttracker.acceptance_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

// application layer imports
import dtu.projecttracker.app.App;
import dtu.projecttracker.app.Project;
import dtu.projecttracker.app.Employee;
import dtu.projecttracker.app.Model;
import dtu.projecttracker.app.OperationNotAllowedException;
import dtu.projecttracker.app.Activity;
import dtu.projecttracker.app.PersonalActivity;
import dtu.projecttracker.app.ProjectActivity;

// @author 
public class ActivitySteps {
	// application later objects
	private Project project;
	private ProjectActivity projectActivity;
	private Model model;
	private App app;
	private PersonalActivity personalActivity;
	private ArrayList<Employee> employees;
	private boolean exists;
	private Project tempProject;

	/*
	 * This constructor uses the principle "dependency injection", and is only
	 * called in the Cucumber libraries.
	 */
	public ActivitySteps(App app) {
		this.app = app;
		this.model = app.getModel();
	}

	// @author Christian
	@Given("^the employee with ID \"([^\"]*)\" is assigned to personal activity with ID \"([^\"]*)\"$")
	public void theEmployeeWithInitialsIsTheOwnerOfThePersonalActivity(String employeeID, String activityID) throws Exception {
		assertTrue(model.getActivity(activityID).hasEmployee(employeeID));
	}

	// @author Christian
	@Given("^the project activity with ID \"([^\"]*)\" doesn't exist$")
	public void theProjectActivityWithIDDoesnTExist(String wrongProjectActivityID) throws Exception {
			assertFalse(model.hasActivity(wrongProjectActivityID));
		
	}

	// @author Christian
	@Given("^the employee has already added \"([^\"]*)\" hours to the activity$")
	public void theEmployeeHasAlreadyAddedHoursToTheActivity(String timeSpentString) throws Exception {
		model.registerHours(app.getActivityID(), timeSpentString);
	}

	// @author Christian
	@Given("^the personal activity with ID \"([^\"]*)\" doesn't exist$")
	public void thePersonalActivityWithIDDoesnTExist(String wrongActivityID) throws Exception {
		assertFalse(model.hasActivity(wrongActivityID));
	}

	// @author Christian
	@Given("^the employee with ID \"([^\"]*)\" is not assigned to activity with ID \"([^\"]*)\"$")
	public void theEmployeeWithIDIsNotAssignedToActivityWithID(String employeeID, String activityID) throws Exception {
			app.setEmployeeID(employeeID);
			app.setActivityID(activityID);
			assertFalse(model.getActivity(activityID).hasEmployee(employeeID));
	}

	// @author Christian
	@Given("^the employee isn’t already added to the activity$")
	public void theEmployeeIsnTAlreadyAddedToTheActivity() throws Exception {
		assertFalse(model.getActivity(app.getActivityID()).hasEmployee(app.getEmployeeID()));
	}

	
	//@author Christian
	@Given("^the project activity with ID \"([^\"]*)\" exists$")
	public void theActivityWithIDExists(String activityID) throws Exception {

		// manually adding the projectActivity to avoid problems regarding
		// logged in status (e.g. the projectActivity exists whether we're logged in or
		// not)
		project = model.getProject(app.getProjectID());
		projectActivity = new ProjectActivity(app);
		projectActivity.setID(activityID);
		projectActivity.setProject(project);
		project.addProjectActivity(projectActivity);
		model.addActivity(projectActivity);
		//this.projectActivityID = activityID;
		app.setActivityID(activityID);
	}

	//@author Christian
	@Given("^the personal activity with ID \"([^\"]*)\" exists$")
	public void thePersonalActivityWithIDExists(String activityID) throws Exception {
		this.personalActivity = model.createPersonalActivity(app.getEmployeeID());
		personalActivity.setID(activityID);
		app.setActivityID(activityID);
	}

	//@author Christian
	@Given("^the employee with ID \"([^\"]*)\" is assigned to project activity with ID \"([^\"]*)\"$")
	public void theEmployeeWithIDIsAssignedToProjectActivityWithID(String employeeID, String activityID) throws Exception {
		model.assignEmployeeToActivity(activityID, employeeID);
	}

	//@author Christian
	@When("^the user changes the name of the activity to \"([^\"]*)\"$")
	public void theUserChangesTheNameOfTheActivityTo(String activityName) throws Exception {
		try {
			model.setActivityName(app.getActivityID(), activityName);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	//@author Christian
	@When("^the user tries to add the employee to the activity$")
	public void theUserTriesToAddTheEmployeeToTheActivity() throws Exception {
		try {
			model.assignEmployeeToActivity(app.getActivityID(), app.getEmployeeID());
		} catch (OperationNotAllowedException e) {
			
		}
	}

	//@author Christian
	@Then("^the name of the activity with ID \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theNameOfTheActivityWithIDIs(String activityID, String activityName) throws Exception {
		assertEquals(activityName, model.getActivity(activityID).getName());
	}

	//@author Christian
	@Then("^the name of the personal activity with ID \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theNameOfThePersonalActivityWithIDIs(String activityID, String activityName) throws Exception {
		personalActivity = (PersonalActivity) model.getActivity(activityID);
		assertEquals(personalActivity.getName(), activityName);
	}

	//@author Christian
	@When("^the user tries to create a project activity$")
	public void theUserTriesToCreateAnActivity() throws Exception {
		try {
			model.createProjectActivity(app.getProjectID());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@author Christian
	@When("^the user tries to add a personal activity$")
	public void theUserTriesToAddAPersonalActivity() throws Exception {
		try {
			model.createPersonalActivity(app.getEmployeeID());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@author Christian
	@When("^the user tries to register \"([^\"]*)\" hours on an activity$")
	public void theUserTriesToRegisterHoursOnAnActivity(String timeSpent) throws Exception {
		try {
			model.getActivity(app.getActivityID()).registerTimeSpent(timeSpent, app.getEmployeeID());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@author Christian
	@When("^the user tries to register \"([^\"]*)\" hours on a personal activity$")
	public void theUserTriesToRegisterHoursOnAPersonalactivity(String timeSpent) throws Exception {
		personalActivity = (PersonalActivity) model.getActivity(app.getActivityID());
		try {
			personalActivity.registerTimeSpent(timeSpent, app.getEmployeeID());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@author Christian
	@When("^the user tries to create a project activity with ID \"([^\"]*)\"$")
	public void theUserTriesToCreateAProjectActivityWithID(String activityID) throws Exception {
		projectActivity = model.getProject(app.getProjectID()).createProjectActivity();
		projectActivity.setID(activityID);
	}

	//@author Christian
	@When("^the user renames the project activity to \"([^\"]*)\"$")
	public void theUserRenamesTheProjectActivityTo(String name) throws Exception {
		try {
			projectActivity = (ProjectActivity) model.getActivity(app.getActivityID());
			projectActivity.setName(name);
		} catch (Exception e) {
			
		}
	}

	//@author Christian
	@When("^the user asks whether the activity exists$")
	public void theUserAsksWhetherTheActivityExists() throws Exception {
		exists = model.hasActivity(app.getActivityID());
	}

	//@author Christian
	@When("^the user asks for the project of the project activity$")
	public void theUserAsksForTheProjectOfTheProjectActivity() throws Exception {
	    	projectActivity = (ProjectActivity) model.getActivity(app.getActivityID());
		    tempProject = projectActivity.getProject();
	}

	//@author Christian
	@Then("^the user finds the project with ID \"([^\"]*)\"$")
	public void theUserFindsTheProjectWithID(String projectID) throws Exception {
	    assertEquals(tempProject.getID(), projectID);
	}

	//@author Christian
	@Then("^the user is told, that the activity exists$")
	public void theUserIsToldThatTheActivityExists() throws Exception {
		assertTrue(exists);
	}

	//@author Christian
	@Then("^the project with ID \"([^\"]*)\" has an activity$")
	public void theProjectWithIDHasAnActivity(String projectID) throws Exception {
		assertFalse(model.getProject(projectID).getActivities().isEmpty());
	}

	//@author Christian
	@Then("^the employee with initials \"([^\"]*)\" has an activity$")
	public void theEmployeeWithInitialsHasAnActivity(String employeeID) throws Exception {
		assertFalse(model.getEmployee(employeeID).getActivities().isEmpty());
	}

	//@author Christian
	@Then("^(\\d+) hours is added to the activity$")
	public void hoursIsAddedToTheActivity(int timeSpent) throws Exception {
		assertEquals(timeSpent, model.getActivity(app.getActivityID()).getSpentTime(app.getEmployeeID()));
	}

	//@author Christian
	@Then("^(\\d+) hours is added to the personal activity$")
	public void hoursIsAddedToThePersonalActivity(int timeSpent) throws Exception {
		assertEquals(model.getActivity(app.getActivityID()).getSpentTime(), timeSpent);
	}

	//@author Christian
	@Then("^the employee with initials \"([^\"]*)\" is added to the activity with ID \"([^\"]*)\"$")
	public void theEmployeeWithInitialsIsAddedToTheActivityWithID(String employeeID, String activityID)
			throws Exception {
		assertTrue(model.getActivity(activityID).hasEmployee(employeeID));
	}

	// FOLLOWING IS NOT YET IMPLEMENTED

	//@author Christian
	@When("^user tries to change the project activity start date to \"([^\"]*)\"$")
	public void userTriesToChangeTheProjectActivityStartDateTo(String startDate) throws Exception {
		try {
			model.setActivityStartDate(app.getActivityID(), startDate);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	//@author Christian
	@Then("^the start date of project activity with ID \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theStartDateOfProjectActivityWithIDIs(String activityID, String startDate) throws Exception {
		assertTrue(model.getActivity(activityID).getStartDateAsString().equals(startDate));
	}

	//@author Christian
	@When("^a user tries to change the end date of the activity to \"([^\"]*)\"$")
	public void aUserTriesToChangeTheProjectActivityEndDateTo(String endDate) throws Exception {
		try {
			//model.getActivity(app.getActivityID()).setEndDate(endDate);
			model.setActivityEndDate(app.getActivityID(), endDate);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	//@author Christian
	@Then("^the end date of the activity with ID \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theEndDateOfProjectActivityWithIDIs(String activityID, String endDate) throws Exception {
		assertTrue(model.getActivity(activityID).getEndDateAsString().equals(endDate));
	}

	//@author Christian
	@Given("^the start date of the project activity is \"([^\"]*)\"$")
	public void theStartDateOfTheProjectActivityIs(String startDate) throws Exception {
		try {
			model.getActivity(app.getActivityID()).setStartDate(startDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@author Christian
	@Given("^the end date of the activity is \"([^\"]*)\"$")
	public void theEndDateOfTheActivityIs(String endDate) throws Exception {
		try {
			model.getActivity(app.getActivityID()).setEndDate(endDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@author Christian
	@Given("^the date \"([^\"]*)\" is before the start time of the activity$")
	public void theDateIsBeforeTheStartTimeOfTheProjectActivity(String dateString) throws Exception {
		assertTrue(model.parseString(dateString).before(model.getActivity(app.getActivityID()).getStartDate()));
	}

	//@author Christian
	@Given("^the date \"([^\"]*)\" is after the end date of the activity$")
	public void theDateIsAfterTheEndDateOfTheActivity(String dateString) throws Exception {
		assertTrue(model.parseString(dateString).after(model.getActivity(app.getActivityID()).getEndDate()));
	}

	//@author Christian
	@When("^a user tries to change the budgeted time to \"([^\"]*)\"$")
	public void aUserTriesToChangeTheBudgetedTimeTo(String budgetedTime) throws Exception {
		model.setActivityBudgetedTime(app.getActivityID(), budgetedTime);
		//model.getActivity(app.getActivityID()).setBudgetedTime(budgetedTime);
	}

	//@author Christian
	@Then("^the budgeted time of the activity with ID \"([^\"]*)\" is (\\d+)$")
	public void theBudgetedTimeOfProjectActivityWithIDIs(String activityID, int budgetedTime) throws Exception {
		assertEquals(model.getActivity(app.getActivityID()).getBudgetedTime(),budgetedTime);
	}

	//@author Christian
	@When("^user tries to change the activity start date to \"([^\"]*)\"$")
	public void userTriesToChangeTheActivityStartDateTo(String startDate) throws Exception {
		try {
			model.getActivity(app.getActivityID()).setStartDate(startDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@author Christian
	@Then("^the start date of activity with ID \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theStartDateOfActivityWithIDIs(String activityID, String startDate) throws Exception {
		assertTrue(model.getActivity(activityID).getStartDateAsString().equals(startDate));
	}

	//@author Christian
	@When("^a user tries to change the activity end date to \"([^\"]*)\"$")
	public void aUserTriesToChangeTheActivityEndDateTo(String endDate) throws Exception {
		try {
			model.getActivity(app.getActivityID()).setEndDate(endDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@author Christian
	@Given("^the start date of the activity is \"([^\"]*)\"$")
	public void theStartDateOfTheActivityIs(String startDate) throws Exception {
		try {
			model.getActivity(app.getActivityID()).setStartDate(startDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	//@author Christian
	@When("^the user asks for the list of employees on the activity$")
	public void theUserAsksForTheListOfEmployeesOnTheActivity() throws Exception {
		employees = model.getActivity(app.getActivityID()).getEmployees();
	}

	//@author Christian
	@Then("^the user finds an employee with ID \"([^\"]*)\"$")
	public void theUserFindsEmployeeWithIDs(String employeeID) throws Exception {
		assertEquals(employees.size(), 1);
		Employee employee1 = employees.get(0);
		assertTrue(employee1.getID().equals(employeeID));
	}

	//@author Christian
	@Then("^the project activity with ID \"([^\"]*)\" now exists$")
	public void theProjectActivityWithIDNowExists(String activityID) throws Exception {
	    assertTrue(model.hasActivity(activityID));
	}

}
