package dtu.projecttracker.acceptance_tests;

import cucumber.api.PendingException;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.projecttracker.app.App;
import dtu.projecttracker.app.Employee;
import dtu.projecttracker.app.Model;
import dtu.projecttracker.app.Project;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import static org.junit.Assert.assertThat;

// @author Asbjørn Olling
public class LoginSteps {
	private App app;
	private Model model;

	// @author Asbjørn 
	public LoginSteps(App app) { // Dependency injection
		this.app = app;
		this.model = app.getModel();
	}

	// SCENARIO 1:
	// @author Asbjørn 
	@Given("^that the user is not logged in$")
	public void thatTheUserIsNotLoggedIn() throws Exception {
		model.setLogInStatus(false);
		model.setLoggedInUser(null);
	}

	// @author Asbjørn 
	@Given("^the user is logged in with initials \"([^\"]*)\"$")
	public void theUserIsLoggedInWithInitials(String employeeID) throws Exception {
		model.setLoggedInUser(model.getEmployee(employeeID));
		model.setLogInStatus(true);
	}

	// @author Asbjørn 
	@When("^user enters \"([^\"]*)\"$")
	public void userEnters(String employeeID) throws Exception {
		app.setEmployeeID(employeeID);
		model.logIn(employeeID);
	}

	// @author Asbjørn 
	@When("^user enters lower case \"([^\"]*)\"$")
	public void userEntersLowerCase(String employeeID) throws Exception {
		app.setEmployeeID(employeeID);
		model.logIn(employeeID);
	}

	// @author Asbjørn 
	@Then("^user login succeeds$")
	public void userLoginSucceeds() throws Exception {
		assertTrue(model.logIn(app.getEmployeeID()));
	}

	// @author Asbjørn 
	@Then("^user is not logged in$")
	public void userIsNotLoggedIn() throws Exception {
		assertFalse(model.getLoginStatus());
	}

	// @author Asbjørn 
	@Then("^user is logged in, using \"([^\"]*)\" as credentials$")
	public void userIsLoggedInUsingAsCredentials(String employeeID) throws Exception {
		assertTrue(model.hasEmployee(employeeID));
		assertEquals(model.getLoggedInUser().getID(), employeeID);
		assertTrue(model.getLoginStatus());
	}
}