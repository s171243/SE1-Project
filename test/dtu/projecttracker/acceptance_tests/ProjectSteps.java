package dtu.projecttracker.acceptance_tests;

// junit4 imports
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

// cucumber imports
import cucumber.api.PendingException;
import cucumber.api.java.en.*;
// application layer imports
import dtu.projecttracker.app.App;
import dtu.projecttracker.app.Employee;
import dtu.projecttracker.app.Project;
import dtu.projecttracker.app.Model;

public class ProjectSteps {
	private final App app;
	private Project project;
	DateFormat format = new SimpleDateFormat("y-M-d", Locale.ENGLISH);
	private Model model;
	protected int budgetedTime;
	private ArrayList<Project> projects;
	private ArrayList<Employee> employees;

	/*
	 * This constructor uses the principle "dependency injection", and is only
	 * called in the Cucumber libraries.
	 * 
	 */
	public ProjectSteps(App app) {
		this.app = app;
		this.model = app.getModel();
		//this.budgetedTime = 0;
	}
	
	// @author David
	@When("^a project is created$")
	public void aProjectIsCreated() {
		project = app.getModel().createProject();
	}
	
	// @author David
	@Then("^the new project exists$")
	public void theNewProjectExists() {
		assertTrue(app.getModel().hasProject(project.getID()));
	}
	
	// @author David
	@Given("^the project with ID \"([^\"]*)\" exists$")
	public void theProjectWithIDExists(String projectID) throws Exception {
		this.project = model.createProject(projectID);
		app.setProjectID(projectID);
	}
	
	// @author Deina
	@Given("^the date \"([^\"]*)\" is before the start time$")
	public void theDateIsBeforeTheStartTime(String dateString) throws Exception {
		assertTrue(model.parseString(dateString).before(project.getStartDate()));
	}
	
	// @author Deina
	@Given("^the end date of the project is \"([^\"]*)\"$")
	public void theEndDateOfTheProjectIs(String endDate) throws Exception {
	   model.getProject(app.getProjectID()).setEndDate(endDate);
	}

	
	// @author David
	@Given("^an employee with ID \"([^\"]*)\" does not exist$")
	public void anEmployeeWithIDDoesNotExist(String wrongEmployeeID) throws Exception {
		app.setEmployeeID(wrongEmployeeID);
		assertFalse(model.hasEmployee(wrongEmployeeID));
	}
	
	// @author David
	@Given("^the employee with initials \"([^\"]*)\" isn’t already added to the project$")
	public void theEmployeeWithInitialsIsnTAlreadyAddedToTheProject(String employeeID) throws Exception {
		app.setEmployeeID(employeeID);
		assertFalse(model.getProject(app.getProjectID()).hasEmployee(employeeID));
	}
	
	// @author David
	@Given("^the project with ID \"([^\"]*)\" doesn't exist$")
	public void theProjectWithIDDoesnTExist(String projectID) throws Exception {
		app.setProjectID(projectID);
		assertFalse(app.getModel().hasProject(projectID));
	}
	
	// @author David
	@Given("^the employee with initials \"([^\"]*)\" is already added to the project$")
	public void theEmployeeWithInitialsIsAlreadyAddedToTheProject(String employeeID) throws Exception {
		try {
			app.setEmployeeID(employeeID);
			//model.getProject(app.getProjectID()).assignEmployee(employeeID);
			model.assignEmployeeToProject(app.getProjectID(), employeeID);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	
	// @author Deina
	@Given("^the start date of the project is \"([^\"]*)\"$")
	public void theStartDateOfTheProjectIs(String dateString) throws Exception {
		try {
			model.setProjectStartDate(app.getProjectID(), dateString);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	// @author David
	@Given("^the projectLeader for the project has ID \"([^\"]*)\"$")
	public void theProjectLeaderForTheProjectHasID(String employeeID) throws Exception {
		project = model.getProject(app.getProjectID());
		app.setEmployeeID(employeeID);
		try {
			project.assignProjectLeader(model.createEmployee(employeeID).getID());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	// @author David
	@When("^the projectLeader is added to the project$")
	public void theProjectLeaderIsAddedToTheProject() throws Exception {
		try {
			model.assignProjectLeader(app.getProjectID(), app.getEmployeeID());
			//model.getProject(app.getProjectID()).setProjectLeader(model.getEmployee(app.getEmployeeID()));
		} catch (Exception e) {

		}
	}
	
	// @author David
	@When("^the employee is added to the project$")
	public void theEmployeeIsAddedToTheProject() throws Exception {
		try {
			project = model.getProject(app.getProjectID());
			this.project.assignEmployee(app.getEmployeeID());
		} catch (Exception e) {

		}
	}
	
	// @author Deina
	@When("^a user tries to change the project start date to \"([^\"]*)\"$")
	public void aUserTriesToChangeTheStartDateTo(String dateString) throws Exception {
		try {
			model.setProjectStartDate(app.getProjectID(), dateString);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// @author Deina
	@Then("^the start date of project with ID \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theStartDateOfProjectWithIDIs(String projectID, String dateString) throws Exception {
		assertEquals(model.getProject(projectID).getStartDate(), model.parseString(dateString));
	}
	
	// @author Deina
	@When("^a user tries to change the project end date to \"([^\"]*)\"$")
	public void aUserTriesToChangeTheProjectEndDateTo(String dateString) throws Exception {
		try {
			model.setProjectEndDate(app.getProjectID(), dateString);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	// @author Deina
	@Then("^the end date of project with ID \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theEndDateOfProjectWithIDIs(String projectID, String dateString) throws Exception {
		assertEquals(model.getProject(projectID).getEndDate(), model.parseString(dateString));
	}
	
	// @author David
	@Then("^the projectLeader with initials \"([^\"]*)\" is assigned to the project with ID \"([^\"]*)\"$")
	public void theProjectLeaderWithInitialsIsAssignedToTheProjectWithID(String employeeID, String projectID)
			throws Exception {
		assertEquals(model.getProject(projectID).getProjectLeader(), model.getEmployee(employeeID));
	}
	
	// @author David
	@Then("^show error \"([^\"]*)\"$")
	public void showError(String errorMessage) throws Exception {
		assertEquals(errorMessage, model.getError());
		model.setError("");
	}
	
	// @author David
	@Then("^the employee with initials \"([^\"]*)\" is added to the project with ID \"([^\"]*)\"$")
	public void theEmployeeWithInitialsIsAddedToTheProjectToProjectWithID(String employeeID, String projectID)
			throws Exception {
		project = model.getProject(projectID);
		assertTrue(project.hasEmployee(employeeID)); // does not work...
	}
	
	// @author David
	// Set project name
	@When("^the user adds a string of name: \"([^\"]*)\"$")
	public void theUserAddsAStringOfName(String projectName) throws Exception {
		try {
			model.setProjectName(app.getProjectID(), projectName);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	// @author Deina
	@When("^the user asks for the list of projects$")
	public void theUserAsksForTheListOfProjects() throws Exception {
	    projects = model.getProjects();
	}
	
	// @author Deina
	@When("^the user asks for the list of projects with the employee$")
	public void theUserAsksForTheListOfProjectsWithTheEmployee() throws Exception {
		projects = model.getEmployee(app.getEmployeeID()).getProjects();
	}
	
	// @author Deina
	@Then("^the user gets the project with ID \"([^\"]*)\"$")
	public void theUserGetsTheProjectWithID(String projectID) throws Exception {
		Project project1 = projects.get(0);
		assertEquals(projectID, project1.getID());
	}
	
	// @author Deina
	@Then("^the user finds projects with IDs \"([^\"]*)\" and \"([^\"]*)\"$")
	public void theUserFindsProjectsWithIDsAnd(String project1ID, String project2ID) throws Exception {
		assertEquals(2, projects.size());
		Project project1 = projects.get(0);
		Project project2 = projects.get(1);
		assertTrue(project1.getID().equals(project1ID) && project2.getID().equals(project2ID)
				|| (project1.getID().equals(project2ID) && project2.getID().equals(project1ID)));
	}
	
	// @author David
	@Then("^the name of project with ID \"([^\"]*)\" to \"([^\"]*)\"$")
	public void theNameOfProjectWithIDTo(String projectID, String projectName) throws Exception {
		assertEquals(model.getProject(projectID).getName(), projectName);
	}
	
	// @author Deina
	@Given("^the budgeted time of project \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theBudgetedTimeOfProjectIs(String projectID, String budgetedTimeString) throws Exception {
		model.setProjectBudgetedTime(projectID, budgetedTimeString);
		//project = model.getProject(projectID);
		//project.setBudgetedTime(budgetedTimeString);
	}
	
	// @author Deina
	@When("^a user tries to change budgeted time to \"([^\"]*)\"$")
	public void aUserTriesToChangeBudgetedTimeTo(String budgetedTimeString) throws Exception {
		model.getProject(app.getProjectID()).setBudgetedTime(budgetedTimeString);
	}
	
	// @author Deina
	@Then("^the budgeted time of project with ID \"([^\"]*)\" is \"([^\"]*)\"$")
	public void theBudgetedTimeOfProjectWithIDIs(String projectID, String budgetedTimeString) throws Exception {
		assertEquals(model.getProject(projectID).getBudgetedTime(), Integer.parseInt(budgetedTimeString));
	}
	
	// @author Deina
	@Then("^the total spent time of the project with ID \"([^\"]*)\" is (\\d+) hours$")
	public void theTotalSpentTimeOfTheProjectWithIDIsHours(String projectID, int budgetedTime) throws Exception {
	    assertEquals(model.getProject(projectID).getSpentTime(), budgetedTime);
	}
	
	// @author Deina
	@When("^the user asks for the list of employees on the project$")
	public void theUserAsksForTheListOfEmployeesOnTheProject() throws Exception {
	    employees = model.getProject(app.getProjectID()).getEmployees();
	}
	
	// @author Deina
	@Then("^the user finds employees with IDs \"([^\"]*)\" and \"([^\"]*)\" on the project with ID$")
	public void theUserFindsEmployeesWithIDsAndOnTheProjectWithID(String employeeID1, String employeeID2) throws Exception {
		assertEquals(2, employees.size());
		Employee employee1 = employees.get(0);
		Employee employee2 = employees.get(1);
		assertTrue(employee1.getID().equals(employeeID1) && employee2.getID().equals(employeeID2)
				|| employee1.getID().equals(employeeID2) && employee2.getID().equals(employeeID1));
	}

} // end class
